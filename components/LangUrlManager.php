<?php

namespace app\components;

use yii\web\UrlManager;
use app\models\Language as Lang;

class LangUrlManager extends UrlManager
{
    /**
     * @param array|string $params
     * @return string
     */
    public function createUrl($params): string
    {
        if (isset($params['lang_id'])) {
            #if language selected try to find it into DB
            $lang = Lang::findOne($params['lang_id']);
            if ($lang === null) {
                $lang = Lang::getDefaultLang();
            }
            unset($params['lang_id']);
        } else {
            #default lang if not selected other
            $lang = Lang::getCurrent();
        }
        #url without prefix
        $url = parent::createUrl($params);

        if ($url[0] == '/') {
            $url = substr($url, 1);
        }
        if ($url == '/') {
            $url = '';
        }
        #add prefix
        if ($lang->default) {
            return $url;
        } else {
            return ($url == '/') ? '/' . $lang->url . '/' : '/' . $lang->url . $url;
        }
    }

}
