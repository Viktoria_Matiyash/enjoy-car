<?php

namespace app\components;

use app\models\Language;
use app\models\Vocabulary;
use app\services\SiteService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Session;

class BaseController extends Controller
{
    private const UTM_TARGETS = [
        'utm_source',
        'Utm_source',
        'Utm_campaign',
        'utm_campaign',
        'Utm_medium',
        'utm_medium',
        'Utm_content',
        'utm_content',
        'utm_term',
        'Utm_term',
        'gclid'
    ];

    /**
     * @var SiteService
     */
    protected $service;

    protected $session;

    public function __construct($id, $module, SiteService $service, Session $session, $config = [])
    {
        $this->service = $service;
        $this->session = $session;
        parent::__construct($id, $module, $config);
    }

    public function init()
    {
        parent::init();
        $this->view->params['isAdminSession'] = $this->session->has('admin_id');
        $queryParams = Yii::$app->request->getQueryParams();
        if (!$this->session->has('targetUrl')) {
            foreach (array_keys($queryParams) as $param) {
                if (in_array($param, self::UTM_TARGETS)) {
                    $this->session->set('targetUrl', Yii::$app->request->absoluteUrl);
                    $this->session->set('saveTargetUrl', false);
                }
            }
        }

        $this->view->params['langId'] = Yii::$app->request->getLangId();
        $this->view->params['langs'] = Language::find()->where(['hide' => 0])->all();
        $this->view->params['lang'] = Language::getCurrent();

        $vocabulary = Vocabulary::find()
            ->select([
                'alias',
                'value_' . $this->view->params['langId'] . ' as value'
            ])
            ->all();
        $vocabulary = ArrayHelper::map($vocabulary, 'alias', 'value');
        $this->view->params['menu'] = $this->service->findMenu();
        $this->view->params = array_merge($this->view->params, $vocabulary);

    }


} 