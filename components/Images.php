<?php

namespace app\components;

class Images
{
    public const FOLDER = 'userfiles/';

    public static function generateFormat(string $name = '')
    {
        $format = explode('.', $name);
        return array_pop($format) . '/';
    }
}