<?php

namespace app\controllers;

use app\services\CalculatorService;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use yii\web\Session;
use yii\web\UnauthorizedHttpException;

class CalculatorController extends Controller
{

    /**
     * @var CalculatorService
     */
    protected $service;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Response
     */
    protected $response;

    /**
     * CalculatorController constructor.
     * @param $id
     * @param $module
     * @param CalculatorService $service
     * @param Request $request
     * @param Session $session
     * @param Response $response
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        CalculatorService $service,
        Request $request,
        Session $session,
        Response $response,
        $config = []
    ) {
        $this->service = $service;
        $this->request = $request;
        $this->session = $session;
        $this->response = $response;
        $this->layout = 'calculator';
        $this->enableCsrfValidation = false;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return string
     * @throws UnauthorizedHttpException
     */
    public function actionIndex()
    {
        if (!$this->session->has('admin_id')) {
            throw new UnauthorizedHttpException();
        }
        $ports = $this->service->findPorts();
        $auctions = $this->service->findAuctions();
        $platforms = $this->service->findPlatforms();
        return $this->render('index', [
            'ports' => $ports,
            'auctions' => $auctions,
            'platforms' => $platforms
        ]);
    }

    /**
     * @return mixed
     * @throws UnauthorizedHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!$this->session->has('admin_id')) {
            throw new UnauthorizedHttpException();
        }
        return $this->asJson($this->service->generatePDF($this->request));
    }
}