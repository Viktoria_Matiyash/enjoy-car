<?php

namespace app\controllers;

use app\components\BaseController;
use app\services\SiteService;
use yii\base\Module;

class SiteController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $sliders = $this->service->findSliders();
        $quotations = $this->service->findQuotation();
        $services = $this->service->findServices();
        $countServices = $this->service->findServiceCounters();
        $models = $this->service->findModels();
        $advantages = $this->service->findAdvantages();
        $stages = $this->service->findStages();
        $teams = $this->service->findTeams();
        $reviews = $this->service->findReviews();
        $modelTypes = $this->service->findModelTypes();

        return $this->render('index', [
            'sliders' => $sliders,
            'quotations' => $quotations,
            'services' => $services,
            'countServices' => $countServices,
            'models' => $models,
            'advantages' => $advantages,
            'stages' => $stages,
            'teams' => $teams,
            'reviews' => $reviews,
            'modelTypes' => $modelTypes
        ]);
    }

}
