<?php

namespace app\controllers;

use app\services\CallbackService;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use yii\web\Session;

class CallbackController extends Controller
{
    /**
     * @var CallbackService
     */
    protected $callbackService;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    protected $session;

    /**
     * CallbackController constructor.
     * @param $id
     * @param $module
     * @param CallbackService $callbackService
     * @param Request $request
     * @param Response $response
     * @param Session $session
     */
    public function __construct(
        $id,
        $module,
        CallbackService $callbackService,
        Request $request,
        Response $response,
        Session $session,
        $config = []
    ) {
        $this->enableCsrfValidation = false;
        $this->request = $request;
        $this->callbackService = $callbackService;
        $this->response = $response;
        $this->session = $session;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $params = $this->request->getBodyParams();

        if ($this->session->has('targetUrl') && $this->session->has('saveTargetUrl') && !$this->session->get('saveTargetUrl')) {
            $params['targetUrl'] = $this->session->get('targetUrl');
            $this->session->set('saveTargetUrl', true);
        }
        $callback = $this->callbackService->createCallback($params);
        $this->response->setStatusCode(201);

        return $this->asJson($callback);
    }
}