<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
$langId = $this->params['langId'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport"
          content="width=device-width height=device-height initial-scale=1.0 maximum-scale=1.0 user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="/img/favicon.png" type="image/x-icon">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->params['title']) ?> </title>
    <meta name="description" content="Как купить авто из США с выгодой до 40% с рынком Украины"/>
    <?php $this->head() ?>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KCMM74G');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KCMM74G"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $this->beginBody() ?>

<div class="page">
    <!-- Page Header-->
    <header class="header js-fixed">
        <div class="header__container">
            <img class="header__logo" src="/img/logo.svg" alt="">
            <div class="header__right">
                <div class="header__all-phone">
                    <a class="header__phone" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['first_phone']) ?>"><?= $this->params['first_phone'] ?></a>
                    <div class="header__dropdown-phone">
                        <a class="header__phone" href="tel:+<?= str_replace([' ', '+', '(', ')'], '',
                            $this->params['second_phone']) ?>"><?= $this->params['second_phone'] ?></a>
                        <a class="header__phone binct-phone-number-1" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                            $this->params['third_phone']) ?>"><?= $this->params['third_phone'] ?></a>
                        <a class="header__phone binct-phone-number-2" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                            $this->params['fourth_phone']) ?>"><?= $this->params['fourth_phone'] ?></a>
<!--                        <a class="header__phone" href="tel:--><?//= str_replace([' ', '+', '(', ')'], '',
//                            $this->params['third_phone']) ?><!--">--><?//= $this->params['third_phone'] ?><!--</a>-->
<!--                        <a class="header__phone" href="tel:--><?//= str_replace([' ', '+', '(', ')'], '',
//                            $this->params['fourth_phone']) ?><!--">--><?//= $this->params['fourth_phone'] ?><!--</a>-->
                    </div>
                </div>
                <div class="header__open-phone">
                    <img class="adaptive-menu__icon" src="/img/phone-call.svg" alt="">
                </div>
                <?php if ($this->params['isAdminSession']): ?>
                    <?php
                    /**
                     * @TODO add style to fine calculator
                     */
                    ?>
                    <a href="/admins/calculator">Калькулятор</a>
                <?php endif; ?>
                <div class="header__socials">
                    <a href="<?= $this->params['facebook_link']; ?>" class="footer__social-link">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path class="fb"
                                  d="M17.3049 0H2.69519C1.20679 0 0.000152588 1.20659 0.000152588 2.69504V17.3049C0.000152588 18.7933 1.20674 19.9999 2.69519 19.9999H9.90064L9.91292 12.853H8.05616C7.81486 12.853 7.61902 12.6579 7.61809 12.4166L7.60918 10.1129C7.60825 9.87025 7.80469 9.67308 8.04731 9.67308H9.90069V7.44709C9.90069 4.86384 11.4784 3.45724 13.7828 3.45724H15.6738C15.9157 3.45724 16.1119 3.65339 16.1119 3.89537V5.83789C16.1119 6.07978 15.9158 6.27587 15.674 6.27602L14.5135 6.27655C13.2603 6.27655 13.0177 6.87207 13.0177 7.74602V9.67313H15.7714C16.0338 9.67313 16.2374 9.90225 16.2064 10.1628L15.9334 12.4666C15.9073 12.687 15.7203 12.8531 15.4984 12.8531H13.0299L13.0177 20H17.305C18.7934 20 20 18.7934 20 17.305V2.69504C20 1.20659 18.7934 0 17.3049 0Z"
                                  fill="white"/>
                        </svg>
                    </a>
                    <a href="<?= $this->params['telegram_link']; ?>" class="footer__social-link">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 300 300"
                             xml:space="preserve" width="20px" height="20px" class="">
                            <path d="M5.299,144.645l69.126,25.8l26.756,86.047c1.712,5.511,8.451,7.548,12.924,3.891l38.532-31.412   c4.039-3.291,9.792-3.455,14.013-0.391l69.498,50.457c4.785,3.478,11.564,0.856,12.764-4.926L299.823,29.22   c1.31-6.316-4.896-11.585-10.91-9.259L5.218,129.402C-1.783,132.102-1.722,142.014,5.299,144.645z M96.869,156.711l135.098-83.207   c2.428-1.491,4.926,1.792,2.841,3.726L123.313,180.87c-3.919,3.648-6.447,8.53-7.163,13.829l-3.798,28.146   c-0.503,3.758-5.782,4.131-6.819,0.494l-14.607-51.325C89.253,166.16,91.691,159.907,96.869,156.711z"
                                  data-original="#000000" class="active-path" fill="#FFFFFF"/>
                        </svg>
                    </a>
                    <a href="<?= $this->params['instagram_link']; ?>" class="footer__social-link">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.0419 0.000976562H5.95811C2.67279 0.000976562 0 2.67377 0 5.95908V14.0429C0 17.3282 2.67279 20.001 5.95811 20.001H14.0419C17.3272 20.001 20 17.3282 20 14.0429V5.95908C20 2.67377 17.3272 0.000976562 14.0419 0.000976562ZM17.988 14.0429C17.988 16.2222 16.2213 17.989 14.0419 17.989H5.95811C3.77875 17.989 2.012 16.2222 2.012 14.0429V5.95908C2.012 3.77969 3.77875 2.01298 5.95811 2.01298H14.0419C16.2213 2.01298 17.988 3.77969 17.988 5.95908V14.0429Z"
                                  fill="white"/>
                            <path d="M9.99998 4.82812C7.14774 4.82812 4.82727 7.1486 4.82727 10.0008C4.82727 12.853 7.14774 15.1735 9.99998 15.1735C12.8522 15.1735 15.1727 12.853 15.1727 10.0008C15.1727 7.14856 12.8522 4.82812 9.99998 4.82812ZM9.99998 13.1615C8.25435 13.1615 6.83927 11.7465 6.83927 10.0008C6.83927 8.25521 8.25439 6.84012 9.99998 6.84012C11.7456 6.84012 13.1607 8.25521 13.1607 10.0008C13.1607 11.7464 11.7456 13.1615 9.99998 13.1615Z"
                                  fill="white"/>
                            <path d="M15.1827 6.10642C15.8673 6.10642 16.4222 5.55148 16.4222 4.86693C16.4222 4.18238 15.8673 3.62744 15.1827 3.62744C14.4982 3.62744 13.9432 4.18238 13.9432 4.86693C13.9432 5.55148 14.4982 6.10642 15.1827 6.10642Z"
                                  fill="white"/>
                        </svg>
                    </a>
                    <a href="https://invite.viber.com/?g2=AQBCtCRJ4j724Uq0YyF0g4tIqkQQj3hX6pb6ODBOfQx6lmapVSIg+Qmy8QNYMWaY" class="footer__social-link">
                        <svg  width="24" height="24" viewBox="0 0 50 50"  fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M44.781 13.156c-.781-2.789-2.328-4.992-4.593-6.55-2.86-1.973-6.149-2.668-8.989-3.094-3.93-.586-7.488-.668-10.883-.254-3.18.39-5.574 1.012-7.757 2.015-4.282 1.97-6.852 5.153-7.637 9.461a58.837 58.837 0 0 0-.79 5.79c-.343 4.171-.03 7.863.954 11.285.96 3.336 2.637 5.718 5.125 7.285.633.398 1.445.687 2.23.965.446.156.88.308 1.235.476.328.153.328.18.324.453-.027 2.371 0 7.02 0 7.02l.008.992h1.781l.29-.281c.19-.18 4.605-4.446 6.179-6.164l.215-.239c.27-.312.27-.312.547-.316 2.125-.043 4.296-.125 6.453-.242 2.613-.14 5.64-.395 8.492-1.582 2.61-1.09 4.515-2.82 5.66-5.14 1.195-2.423 1.902-5.044 2.168-8.016.469-5.227.137-9.762-1.012-13.864zM35.383 33.48c-.656 1.067-1.633 1.81-2.785 2.29-.844.351-1.703.277-2.535-.075-6.965-2.949-12.43-7.593-16.04-14.273-.746-1.375-1.261-2.875-1.855-4.328-.121-.297-.113-.649-.168-.977.05-2.347 1.852-3.672 3.672-4.07.695-.156 1.312.09 1.828.586a16.005 16.005 0 0 1 3.41 4.715c.371.777.203 1.465-.43 2.043-.132.12-.27.23-.414.34-1.445 1.085-1.656 1.91-.886 3.546 1.312 2.785 3.492 4.657 6.308 5.817.742.304 1.442.152 2.008-.45.078-.078.164-.156.219-.25 1.11-1.851 2.723-1.667 4.21-.613.977.696 1.927 1.43 2.891 2.137 1.473 1.082 1.461 2.098.567 3.562zM26.145 15c-.329 0-.657.027-.98.082a.999.999 0 1 1-.328-1.973c.429-.074.87-.109 1.308-.109C30.477 13 34 16.523 34 20.855c0 .442-.035.883-.11 1.31a1 1 0 0 1-1.972-.33A5.865 5.865 0 0 0 26.145 15zM31 21c0 .55-.45 1-1 1s-1-.45-1-1c0-1.652-1.348-3-3-3-.55 0-1-.45-1-1s.45-1 1-1c2.758 0 5 2.242 5 5zm5.71 2.223a1 1 0 0 1-1.952-.446 8.875 8.875 0 0 0 .219-1.96c0-4.86-3.957-8.817-8.817-8.817-.664 0-1.324.074-1.96.219a.996.996 0 0 1-1.196-.754.996.996 0 0 1 .754-1.195c.781-.18 1.59-.27 2.402-.27 5.965 0 10.817 4.852 10.817 10.816 0 .813-.09 1.622-.266 2.407z" fill="white"/>
                        </svg>
                    </a>
                </div>
                <div class="header__adaptive-phone">
                    <a class="header__phone binct-phone-number-1" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['first_phone']) ?>"><?= $this->params['first_phone'] ?></a>
                    <a class="header__phone binct-phone-number-2" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['second_phone']) ?>"><?= $this->params['second_phone'] ?></a>
<!--                    <a class="header__phone" href="tel:--><?//= str_replace([' ', '+', '(', ')'], '',
//                        $this->params['third_phone']) ?><!--">--><?//= $this->params['third_phone'] ?><!--</a>-->
<!--                    <a class="header__phone" href="tel:--><?//= str_replace([' ', '+', '(', ')'], '',
//                        $this->params['fourth_phone']) ?><!--">--><?//= $this->params['fourth_phone'] ?><!--</a>-->
                </div>
                <div class="header__dropdown">
                    <div class="header__select">
                        <span class="header__span"><?= $this->params['lang']->getName() ?></span>
                    </div>
                    <ul class="header__dropdown-menu">
                        <?php foreach ($this->params['langs'] as $lang): ?>
                            <?php if (false) : ?><a href="/<?= $lang->getUrl() ?>"></a><?php endif; ?>
                            <?php if ($lang->getId() !== $this->params['lang']->getId()) : ?><a
                                href="/<?= $lang->getPrepareUrl() ?>">
                                <li><?= $lang->getName() ?></li></a><?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="adaptive-menu">
                    <div class="adaptive-menu__open-menu"><img class="adaptive-menu__icon" src="/img/menu.svg" alt="">
                    </div>
                    <div class="adaptive-menu__full-menu">
                        <a class="adaptive-menu__close-menu"><img src="/img/x.svg" alt=""></a>
                        <nav class="adaptive-menu__menu-top">
                            <?php foreach ($this->params['menu'] as $menu): ?>
                                <a class="adaptive-menu__link"
                                   href="#<?= $menu->getAlias() ?>"><?= $menu->getName($this->params['langId']) ?></a>
                            <?php endforeach; ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?= $content ?>
    <footer class="footer">
        <div class="footer__container">
            <div class="footer__top">
                <img class="footer__logo" src="/img/logo.svg" alt="">
                <nav class="footer__menu">
                    <?php foreach ($this->params['menu'] as $menu): ?>
                        <a class="footer__link"
                           href="#<?= $menu->getAlias() ?>"><?= $menu->getName($this->params['langId']) ?></a>
                    <?php endforeach; ?>
                </nav>
            </div>
            <div class="footer__bottom">
                <div class="footer__socials">
                    <a href="<?= $this->params['facebook_link']; ?>" class="footer__social-link">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path class="fb"
                                  d="M17.3049 0H2.69519C1.20679 0 0.000152588 1.20659 0.000152588 2.69504V17.3049C0.000152588 18.7933 1.20674 19.9999 2.69519 19.9999H9.90064L9.91292 12.853H8.05616C7.81486 12.853 7.61902 12.6579 7.61809 12.4166L7.60918 10.1129C7.60825 9.87025 7.80469 9.67308 8.04731 9.67308H9.90069V7.44709C9.90069 4.86384 11.4784 3.45724 13.7828 3.45724H15.6738C15.9157 3.45724 16.1119 3.65339 16.1119 3.89537V5.83789C16.1119 6.07978 15.9158 6.27587 15.674 6.27602L14.5135 6.27655C13.2603 6.27655 13.0177 6.87207 13.0177 7.74602V9.67313H15.7714C16.0338 9.67313 16.2374 9.90225 16.2064 10.1628L15.9334 12.4666C15.9073 12.687 15.7203 12.8531 15.4984 12.8531H13.0299L13.0177 20H17.305C18.7934 20 20 18.7934 20 17.305V2.69504C20 1.20659 18.7934 0 17.3049 0Z"
                                  fill="white"/>
                        </svg>
                    </a>
                    <a href="<?= $this->params['youtube_link']; ?>" class="footer__social-link">
                        <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.8463 0.924805H4.15374C1.85969 0.924805 0 2.7845 0 5.07855V10.9236C0 13.2176 1.85969 15.0773 4.15374 15.0773H15.8463C18.1403 15.0773 20 13.2176 20 10.9236V5.07855C20 2.7845 18.1403 0.924805 15.8463 0.924805ZM13.0371 8.28544L7.56814 10.8938C7.42241 10.9633 7.25408 10.8571 7.25408 10.6956V5.31586C7.25408 5.15213 7.42684 5.04601 7.57287 5.12002L13.0418 7.89143C13.2044 7.97381 13.2016 8.207 13.0371 8.28544Z"
                                  fill="white"/>
                        </svg>
                    </a>
                    <a href="<?= $this->params['telegram_link']; ?>" class="footer__social-link">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 300 300"
                             xml:space="preserve" width="20px" height="20px" class="">
                            <path d="M5.299,144.645l69.126,25.8l26.756,86.047c1.712,5.511,8.451,7.548,12.924,3.891l38.532-31.412   c4.039-3.291,9.792-3.455,14.013-0.391l69.498,50.457c4.785,3.478,11.564,0.856,12.764-4.926L299.823,29.22   c1.31-6.316-4.896-11.585-10.91-9.259L5.218,129.402C-1.783,132.102-1.722,142.014,5.299,144.645z M96.869,156.711l135.098-83.207   c2.428-1.491,4.926,1.792,2.841,3.726L123.313,180.87c-3.919,3.648-6.447,8.53-7.163,13.829l-3.798,28.146   c-0.503,3.758-5.782,4.131-6.819,0.494l-14.607-51.325C89.253,166.16,91.691,159.907,96.869,156.711z"
                                  data-original="#000000" class="active-path" fill="#FFFFFF"/>
                        </svg>
                    </a>
                    <a href="<?= $this->params['instagram_link']; ?>" class="footer__social-link">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.0419 0.000976562H5.95811C2.67279 0.000976562 0 2.67377 0 5.95908V14.0429C0 17.3282 2.67279 20.001 5.95811 20.001H14.0419C17.3272 20.001 20 17.3282 20 14.0429V5.95908C20 2.67377 17.3272 0.000976562 14.0419 0.000976562ZM17.988 14.0429C17.988 16.2222 16.2213 17.989 14.0419 17.989H5.95811C3.77875 17.989 2.012 16.2222 2.012 14.0429V5.95908C2.012 3.77969 3.77875 2.01298 5.95811 2.01298H14.0419C16.2213 2.01298 17.988 3.77969 17.988 5.95908V14.0429Z"
                                  fill="white"/>
                            <path d="M9.99998 4.82812C7.14774 4.82812 4.82727 7.1486 4.82727 10.0008C4.82727 12.853 7.14774 15.1735 9.99998 15.1735C12.8522 15.1735 15.1727 12.853 15.1727 10.0008C15.1727 7.14856 12.8522 4.82812 9.99998 4.82812ZM9.99998 13.1615C8.25435 13.1615 6.83927 11.7465 6.83927 10.0008C6.83927 8.25521 8.25439 6.84012 9.99998 6.84012C11.7456 6.84012 13.1607 8.25521 13.1607 10.0008C13.1607 11.7464 11.7456 13.1615 9.99998 13.1615Z"
                                  fill="white"/>
                            <path d="M15.1827 6.10642C15.8673 6.10642 16.4222 5.55148 16.4222 4.86693C16.4222 4.18238 15.8673 3.62744 15.1827 3.62744C14.4982 3.62744 13.9432 4.18238 13.9432 4.86693C13.9432 5.55148 14.4982 6.10642 15.1827 6.10642Z"
                                  fill="white"/>
                        </svg>
                    </a>
                    <a href="https://invite.viber.com/?g2=AQBCtCRJ4j724Uq0YyF0g4tIqkQQj3hX6pb6ODBOfQx6lmapVSIg+Qmy8QNYMWaY" class="footer__social-link">
                        <svg  width="24" height="24" viewBox="0 0 50 50"  fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M44.781 13.156c-.781-2.789-2.328-4.992-4.593-6.55-2.86-1.973-6.149-2.668-8.989-3.094-3.93-.586-7.488-.668-10.883-.254-3.18.39-5.574 1.012-7.757 2.015-4.282 1.97-6.852 5.153-7.637 9.461a58.837 58.837 0 0 0-.79 5.79c-.343 4.171-.03 7.863.954 11.285.96 3.336 2.637 5.718 5.125 7.285.633.398 1.445.687 2.23.965.446.156.88.308 1.235.476.328.153.328.18.324.453-.027 2.371 0 7.02 0 7.02l.008.992h1.781l.29-.281c.19-.18 4.605-4.446 6.179-6.164l.215-.239c.27-.312.27-.312.547-.316 2.125-.043 4.296-.125 6.453-.242 2.613-.14 5.64-.395 8.492-1.582 2.61-1.09 4.515-2.82 5.66-5.14 1.195-2.423 1.902-5.044 2.168-8.016.469-5.227.137-9.762-1.012-13.864zM35.383 33.48c-.656 1.067-1.633 1.81-2.785 2.29-.844.351-1.703.277-2.535-.075-6.965-2.949-12.43-7.593-16.04-14.273-.746-1.375-1.261-2.875-1.855-4.328-.121-.297-.113-.649-.168-.977.05-2.347 1.852-3.672 3.672-4.07.695-.156 1.312.09 1.828.586a16.005 16.005 0 0 1 3.41 4.715c.371.777.203 1.465-.43 2.043-.132.12-.27.23-.414.34-1.445 1.085-1.656 1.91-.886 3.546 1.312 2.785 3.492 4.657 6.308 5.817.742.304 1.442.152 2.008-.45.078-.078.164-.156.219-.25 1.11-1.851 2.723-1.667 4.21-.613.977.696 1.927 1.43 2.891 2.137 1.473 1.082 1.461 2.098.567 3.562zM26.145 15c-.329 0-.657.027-.98.082a.999.999 0 1 1-.328-1.973c.429-.074.87-.109 1.308-.109C30.477 13 34 16.523 34 20.855c0 .442-.035.883-.11 1.31a1 1 0 0 1-1.972-.33A5.865 5.865 0 0 0 26.145 15zM31 21c0 .55-.45 1-1 1s-1-.45-1-1c0-1.652-1.348-3-3-3-.55 0-1-.45-1-1s.45-1 1-1c2.758 0 5 2.242 5 5zm5.71 2.223a1 1 0 0 1-1.952-.446 8.875 8.875 0 0 0 .219-1.96c0-4.86-3.957-8.817-8.817-8.817-.664 0-1.324.074-1.96.219a.996.996 0 0 1-1.196-.754.996.996 0 0 1 .754-1.195c.781-.18 1.59-.27 2.402-.27 5.965 0 10.817 4.852 10.817 10.816 0 .813-.09 1.622-.266 2.407z" fill="white"/>
                        </svg>
                    </a>
                </div>
                <p class="footer__copywrite"><?= $this->params['all_defence'] ?></p>
            </div>
        </div>
    </footer>
</div>


<?php $this->endBody() ?>
<script type="text/javascript">
    (function(d, w, s) {
        var widgetHash = 'clzgfi2iobmv3d6rsxsx', ctw = d.createElement(s); ctw.type = 'text/javascript'; ctw.async = true;
        ctw.src = '//widgets.binotel.com/calltracking/widgets/'+ widgetHash +'.js';
        var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(ctw, sn);
    })(document, window, 'script');
</script>
</body>
</html>
<?php $this->endPage() ?>
