<?php


/* @var $this \yii\web\View */
/* @var $sliders \app\models\Slider[]|array */
/* @var $quotations \app\models\Quotation[]|array */
/* @var $services \app\models\Service[]|array */
/* @var $countServices \app\models\ServiceCounter[]|array */
/* @var $models \app\models\Model[]|array */
/* @var $advantages \app\models\Advantage[]|array */
/* @var $stages \app\models\Stage[]|array */
/* @var $teams \app\models\Team[]|array */
/* @var $modelTypes \app\models\ModelType[]|array */
/* @var $reviews \app\models\Review[]|array */
?>
<main class="main">
    <section id="main-block" class="main-block">
        <?php foreach ($sliders as $slider): ?>
            <div class="main-block__layout" style="background-image: url('img/bg-intro.png')">
                <div class="main-block__container">
                    <div class="main-block__left">
                        <h1 class="main-block__caption"><?=$this->params['we_delivery_max']?></h1>
                        <div class="main-block__widgets">

                            <div class="main-block__icon-box">
                                <div class="main-block__icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="59px" height="56px" x="0px" y="0px" viewBox="0 0 505 491">
                                        <style type="text/css">
                                            .st2{fill:none;stroke:#EEBE2C;stroke-width:12;stroke-miterlimit:10;}
                                        </style>
                                        <path d="M451.9,225.5l-0.9-0.3c-13.1-3.8-26.8,3.8-30.6,16.8c-5.6,19.5-15.3,52.3-18.2,62.3l-35.1,26.2l-6-2.7 l21.8-22.2c9-9.1,9.3-23.6,0.9-33.3c-8.5-9.8-23.2-11-33.3-3.1l-58.1,44.4c-4.5,3.4-8.1,8-10.1,13.2l-31,98.7l83.7,33l13.2-54.2 l84-49.2c6.2-3.7,10.9-9.6,12.9-16.6l23.1-83.6V255C471.8,242.4,464.5,229.3,451.9,225.5z M450.9,250.5l-23,83.2 c-0.8,2.5-2.5,4.8-4.8,6.2l-90.6,53l-10.1,41.5l-48.8-19.2l25.6-81.8c0.9-2,2.3-3.8,4-5.2l58.4-44.6l0.1-0.1 c2.5-2,6.2-1.7,8.4,0.7c2.3,2.4,2.2,6.2-0.2,8.7L330,333.5l39.2,18l48.6-36.2l0.8-2.9c0.1-0.5,12.3-42.1,19-65.3 c1-3.3,4.5-5.3,7.9-4.5l0.7,0.3C449.9,243.6,451.9,247.2,450.9,250.5z" fill="#ffffff"/>
                                        <path d="M217.2,327.3l-0.5-0.5c-2.1-5.2-5.6-9.9-10.1-13.2l-58.1-44.4c-10.1-8.1-24.8-6.7-33.3,3.1 c-8.4,9.6-8.1,24.1,0.9,33.2l21.9,22.3l-6,2.7l-35.1-26.2c-2.9-9.9-12.5-42.7-18.1-62.2c-3.8-13.1-17.5-20.6-30.6-16.9l-0.9,0.3 c-12.6,3.8-19.8,16.8-16.5,29.6l23.1,83.5l0.1,0.2c2.1,7,6.7,12.9,13,16.6l84,49.1l13.2,54.2l83.7-33L217.2,327.3z M166.6,392.9 l-90.6-53c-2.4-1.4-4-3.6-4.8-6.2l-23-83.2c-0.9-3.3,1.1-6.9,4.4-7.8l0.7-0.3c3.4-0.8,6.9,1.3,7.9,4.6 c6.7,23.1,18.9,64.8,19,65.2l0.8,2.9l48.6,36.2l39.2-18l-40-40.6c-2.3-2.4-2.4-6.2-0.1-8.6c2.2-2.4,5.9-2.7,8.4-0.7l58.3,44.6 l0.2,0.1c1.7,1.4,3.2,3.1,4,5.2l25.6,81.8l-48.5,19.3L166.6,392.9z" fill="#ffffff"/>
                                        <path d="M379,108.2l-5-4.6c0-0.1-0.1-0.2-0.2-0.3L353,54.8c-4.4-10.3-14.7-17-25.9-16.9H175.7 c-11.2-0.1-21.4,6.6-25.9,16.9l-20.5,47.8l-7.8,6.3c-8.3,7-13.2,17.3-13.3,28.2v82.7c0.2,15.3,13,28,28.2,28H152 c15.4-0.1,27.8-12.6,27.9-28v-8.2h139.4v8.2c0.1,15.4,12.5,27.8,27.9,28h15.6c15.3,0,28.1-12.7,28.1-28V135 C390.7,124.8,386.4,115.1,379,108.2z M166.6,61.9c1.6-3.6,5.2-6,9.2-5.9h151.3c4-0.1,7.6,2.3,9.2,5.9l15.4,35.8H346 c-5-23.5-28.1-38.6-51.6-33.6c-16.8,3.5-30,16.7-33.6,33.6H151.1L166.6,61.9z M327.1,97.7h-47.5c3.6-9.1,12.9-16.4,23.8-16.4 C314.3,81.3,323.4,88.6,327.1,97.7z M161.8,219.9c-0.1,5.3-4.4,9.8-9.8,9.9h-15.6c-5.4-0.1-9.8-4.4-10-9.9v-10.3 c3.6,1.1,6.6,2.1,10,2.1h25.3V219.9z M372.7,220c-0.2,5.3-4.5,9.7-10,9.8h-15.6c-5.3-0.1-9.7-4.4-9.8-9.9v-8.2h25.3 c3.3,0,6.3-1,10-2.1V220z M372.7,183.6c-0.2,5.4-4.5,9.9-10,10H136.4c-5.4-0.2-9.9-4.5-10-10v-46.4c0.1-5.6,2.6-10.9,6.9-14.5 l8.3-7h218.9l5.9,5.5c3.8,3.6,6.1,8.6,6.2,13.8L372.7,183.6L372.7,183.6z" fill="#EEBE2C"/>
                                        <line class="st2" x1="182.8" y1="143.4" x2="312.8" y2="143.4"/>
                                        <line class="st2" x1="182.5" y1="169.8" x2="312.5" y2="169.8"/>
                                    </svg>
                                </div>
                                <div class="main-block__icon-text"><?=$this->params['10+stages']?> </div>
                            </div>

                            <div class="main-block__icon-box">
                                <div class="main-block__icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="49px" height="56px" x="0px" y="0px" viewBox="0 0 505 491">
                                        <style type="text/css">
                                            .st0{fill:none;stroke:#EEBE2C;stroke-width:12;stroke-miterlimit:10;}
                                            .st1{fill:none;stroke:#EEBE2C;stroke-width:15;stroke-miterlimit:10;}
                                            .st2{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;}
                                            .st3{font-family:'Roboto-Black';}
                                            .st4{font-size:42.7315px;}
                                            .st5{letter-spacing:2;}
                                            .st6{fill:#FFFFFF;}
                                            .st7{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:3;stroke-miterlimit:10;}
                                            .st8{fill:#FFFFFF;stroke:#FFFFFF;stroke-miterlimit:10;}
                                        </style>
                                        <line class="st0" x1="134" y1="146.6" x2="356.5" y2="146.6"/>
                                        <line class="st0" x1="134" y1="182" x2="356.5" y2="182"/>
                                        <path class="st1" d="M375.7,429.9H119.2c-15,0-27.2-12.2-27.2-27.2V50.4c0-15,12.2-27.2,27.2-27.2h256.6c15,0,27.2,12.2,27.2,27.2 v352.3C402.9,417.7,390.8,429.9,375.7,429.9z"/>
                                        <line class="st0" x1="134" y1="217.3" x2="356.5" y2="217.3"/>
                                        <line class="st0" x1="134" y1="252.7" x2="356.5" y2="252.7"/>
                                        <line class="st0" x1="134" y1="288.1" x2="356.5" y2="288.1"/>
                                        <line class="st0" x1="135.5" y1="323.4" x2="282.8" y2="323.4"/>
                                        <text transform="matrix(1 0 0 1 122.0568 103.7697)" class="st2 st3 st4 st5">CONTRACT</text>
                                        <path class="st6" d="M298.6,387.3c0.5,3.2,2.2,4.2,4.4,5.3c2.1,1,7.5-3.9,9.2-4.8c10.6-6,25,1,36.4,1c6.7,0,3.8-9.1-2.9-9.1 c-13.3,0-25.8-0.9-38.9,1.6c0-0.9,1.2-5,0.4-6.6c-2.5-4.9-5.6-3.3-8.7-2.7c-0.2-0.6,1.1-4.1,0.8-4.7c-0.8-1.6-4.9,0.6-6.2,0.3 c3.7-7.4,7.5-18.2,5.1-24.5c-0.7-1.9-2.8-2.9-4.5-3c-7-0.3-12.9,8.2-17.4,13.2c-11.2,12.7-21.1,27.1-30.6,41.5 c-4.1,6.2,3.5,10.7,7.5,4.6c8.5-12.7,23.3-31,33-42.5c-2.3,4.4-7.7,16.1-10.3,20.3c-3.7,6.6,3.7,12.3,7.5,6.1 c0.9-1.6,3.6-5.7,4.8-6.7c-0.3,0.7-2,4.4-2.2,5.2l0,0c-0.2,0.5-0.4,1-0.6,1.4c-2.3,6.8,4.9,9.9,8.1,4.6c0.8-1.3,4.5-7.1,5.2-8.6 C298.4,380.2,298.4,385.9,298.6,387.3z"/>
                                        <polygon class="st7" points="373.4,347.1 381.8,349.6 425.1,207.5 396.2,198.7 352.7,341.1 361.1,343.5"/>
                                        <path class="st8" d="M358.3,380.6l3.8-4.4l-7.9-2.5l0.8,5.8C355.3,381.9,356.8,382.4,358.3,380.6z"/>
                                        <polygon class="st2" points="377.4,358 378.8,353.6 353.1,346.1 351.7,350.7 353.8,368.1 365.3,371.4"/>
                                        <path class="st7" d="M426.3,203.8l2.9,0.9c1.5-5.1,3-10.4,4.5-16.1l1.9,0.6l1.9,0.6l2,0.6l-17.3,56.9c-0.6,2.1,0.5,4.3,2.6,4.9 l0.8,0.2c2.1,0.6,4.3-0.5,4.9-2.6l25.3-83.3c0.6-2.1-0.5-4.3-2.6-4.9l-0.8-0.3c-2.1-0.6-4.3,0.5-4.9,2.6l-3.5,11.6l-2.7-0.7 l-1.9-0.6l-1.9-0.6c2.2-9.1,4.4-18.4,6.3-27.7c1.9-8.8,3.5-17.5,5-25.7c2.8-16.3,4.5-30.6,4.5-40l-8.9-2.7 c-5.3,7.8-11.8,20.7-18.6,35.9c-4.8,10.7-9.6,22.6-14.3,34.5c-6.2,16-11.9,32.2-16.2,46.3l1.9,0.6L426.3,203.8z"/>
                                    </svg>
                                </div>
                                <div class="main-block__icon-text"><?=$this->params['garantee_contract']?></div>
                            </div>

                            <div class="main-block__icon-box">
                                <div class="main-block__icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="58px" height="58px" x="0px" y="0px" viewBox="0 0 505 491">
                                        <path class="st0" d="M183.8,129.2c0,48.3,39.3,87.6,87.6,87.6s87.6-39.3,87.6-87.6s-39.3-87.6-87.6-87.6S183.8,81,183.8,129.2z M343,129.2c0,39.5-32.1,71.6-71.6,71.6s-71.6-32.1-71.6-71.6c0-39.5,32.1-71.6,71.6-71.6S343,89.7,343,129.2z" fill="#FFFFFF"/>
                                        <path class="st1" d="M125.8,447c3.1,3.1,8.1,3.1,11.2,0.1l38.6-38c6.6-6.6,8.5-16.3,5.7-24.7l8.3-8c4.5-4.3,10.4-6.7,16.6-6.7H312 c18.8,0,36.5-7.2,50-20.3c0.6-0.5-4.2,5.1,72.2-86.2c11.3-13.4,9.6-33.5-3.8-44.9c-13.3-11.2-33.3-9.6-44.7,3.6l-47,48.3 c-5.9-7.3-15-11.8-24.7-11.8h-88.8c-12.6-5.3-26-8-39.9-8c-38.3,0-71.8,17.7-89.8,50.9c-7.6-1.4-15.5,0.9-21.3,6.6l-37.9,38 c-3.1,3.1-3.1,8.1,0,11.2L125.8,447z M185.4,266.1c12.2,0,24,2.5,35,7.3c1,0.4,2.1,0.7,3.2,0.7H314c8.6,0,15.9,7,15.9,15.9 c0,8.8-7.1,15.9-15.9,15.9h-64.9c-4.4,0-8,3.6-8,8c0,4.4,3.6,8,8,8H314c17.6,0,31.8-14.3,31.8-31.8c0-1.4-0.1-2.8-0.3-4.2 c45.4-46.7,51.8-53.3,52.1-53.6c5.7-6.7,15.7-7.5,22.4-1.9c6.7,5.7,7.6,15.7,1.9,22.5l-71.4,85.3c-10.4,10-24.1,15.5-38.6,15.5 H206.2c-10.4,0-20.2,4-27.6,11.2l-6.8,6.5L109.5,309C124,281.7,151.4,266.1,185.4,266.1z M85.6,319c2.6-2.6,6.7-3.1,9.9-1.1 c1.4,0.8-2.6-2.8,68.9,68.7c3.2,3.2,3,8.3,0,11.2l-32.9,32.4l-78.2-78.8L85.6,319z" fill="#EEBE2C"/>
                                        <path class="st0" d="M261.8,81.5v9.3c-9.3,3.3-15.9,12.1-15.9,22.5c0,13.2,10.7,23.9,23.9,23.9c4.4,0,8,3.6,8,8s-3.6,8-8,8 c-3.4,0-7.1-2.1-10.3-6c-2.8-3.4-7.8-3.8-11.2-1c-3.4,2.8-3.8,7.8-1,11.2c4.3,5.1,9.3,8.6,14.6,10.4v9.3c0,4.4,3.6,8,8,8s8-3.6,8-8 v-9.3c9.3-3.3,15.9-12.1,15.9-22.5c0-13.2-10.7-23.9-23.9-23.9c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c2.8,0,5.8,1.4,8.6,4.2 c3.2,3.1,8.2,3,11.3-0.2c3.1-3.2,3-8.2-0.2-11.3c-4-3.9-8.1-6.1-11.7-7.3v-9.3c0-4.4-3.6-8-8-8S261.8,77.1,261.8,81.5z" fill="#FFFFFF"/></g>
                                    </svg>
                                </div>
                                <div class="main-block__icon-text"><?=$this->params['good_price']?></div>
                            </div>
                        </div>
                    </div>
                    <div class="main-block__right">
                      <div class="main-block__form-container">

                        <div class="main-block__form-inner">
                            <h2 class="main-block__form-caption"><?= $this->params['get_good_price_by_3_cars'];?></h2>
                            <form class="main-block__form-request" action="" method="post" id="main-form-request">

                                <div class="field-wrap">
                                    <input id="main-form-filed" class="field-input phone-mask clear" type="tel" name="phone" required="" autocomplete="off" placeholder="Телефон" minlength="17" maxlength="17">
                                    <label class="field-input-label" for="main-form-filed">
                                        <img src="img/telephone.svg" width="24px" height="24px">
                                    </label>
                                </div>
                                
                                <button id="main-form-submit" type="submit" class="button button-block">Отправить</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        <!-- <?php endforeach; ?>
        <div class="fadeOut owl-carousel owl-theme">
            <?php foreach ($sliders as $slider): ?>
                <div class="item" style="background-image: url(<?= $slider->getBackground() ?>)">
                    <div class="main-block__container">
                        <div class="main-block__left">
                            <h1 class="main-block__caption">
                                <?= $slider->getName($this->params['langId']) ?>
                            </h1>
                            <p class="main-block__description"><?= $slider->getDescription($this->params['langId']) ?></p>
                            <a class="main-block__callback js-popup" href="#"><?= $this->params['get_call_1'] ?></a>
                        </div>
                        <div class="main-block__right">
                            <img class="main-block__img revealator-slideleft revealator-once"
                                 src="<?= $slider->getCatImg() ?>"
                                 alt="<?= $slider->getName($this->params['langId']) ?>">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?> -->
        </div>
    </section>
    <section id="video" class="video">
        <div class="video__container">
            <a class="video__link" href="<?= $this->params['video__link']?>" data-fancybox="html5-video">
                <img class="video__poster" src="/img/maxresdefault.jpg" alt="Image description">
                <svg class="video__icon" width="111" height="111" viewBox="0 0 111 111" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="55.5" cy="55.5" r="55.5" fill="#424242"></circle>
                    <path d="M38.75 26.4881L89 55.5L38.75 84.5118L38.75 26.4881Z" stroke="white" stroke-width="2"></path>
                </svg>
            </a>
        </div>
    </section>
    <section id="feedback" class="feedback">
        <div class="feedback__container">
            <h2 class="feedback__caption"><?= $this->params['good_text'] ?></h2>
            <form class="subscription__form" action="#">
                <div class="subscription__field">
                    <input class="subscription__name clear" type="text" name="name" placeholder="<?= $this->params['name'] ?>"
                           pattern="[А-я]{3,}" required>
                    <span class="subscription__error"><?= $this->params['validate_name'] ?></span>
                </div>
                <div class="subscription__field">
                    <input class="subscription__phone phone-mask clear" type="tel" name="phone" placeholder="Телефон:"
                           minlength="17" maxlength="17"
                           required>
                    <span class="subscription__error"><?= $this->params['validate_phone'] ?></span>
                </div>
                <p class="subscription__notification"><?= $this->params['message_sent'] ?></p>
                <input class="subscription__send" id="feedback-submit" type="submit" value="<?= $this->params['button'] ?>">
            </form>
        </div>
    </section>
    <section id="advantages" class="advantages">
        <div class="advantages__container">
            <h2 class="advantages__caption"><?= $this->params['why_in_us'] ?></h2>
            <?php foreach ($advantages as $advantage): ?>
                <div class="advantages__item adaptive--hide">
                    <img src="<?= $advantage->getImg(); ?>" alt="<?= $advantage->getName($this->params['langId']); ?>"
                         class="advantages__img">
                    <h3 class="advantages__title"><?= $advantage->getName($this->params['langId']); ?></h3>
                    <p class="advantages__desc"><?= $advantage->getDescription($this->params['langId']); ?></p>
                </div>
            <?php endforeach; ?>
            <div class="advantages__slider adaptive--show owl-carousel owl-theme">
                <?php foreach ($advantages as $advantage): ?>
                    <div class="advantages__item item">
                        <img src="<?= $advantage->getImg(); ?>" alt="<?= $advantage->getName($this->params['langId']); ?>"
                             class="advantages__img">
                        <h3 class="advantages__title"><?= $advantage->getName($this->params['langId']); ?></h3>
                        <p class="advantages__desc"><?= $advantage->getDescription($this->params['langId']); ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section id="popular" class="popular">
        <div class="popular__container">
            <h2 class="popular__caption"><?= $this->params['popular_models']; ?></h2>
            <div class="popular__slider owl-carousel owl-theme">
                <?php foreach ($models as $key => $model): ?>
                    <?php if ($key % 4 == 0) : ?>
                        <div class="popular__slide item">
                    <?php endif; ?>
                    <div class="popular__item" data-modal="<?= $model->getName($this->params['langId']) ?>">
                        <img class="popular__img" src="<?= $model->getImg() ?>"
                             alt="<?= $model->getName($this->params['langId']) ?>">
                        <div class="popular__hide">
                            <div class="popular__block">
                                <div class="popular__content">
                                    <?php if (!empty($model->getAuctionCostPrice())): ?>
                                        <div>
                                            <span><?= $this->params['cost_auto_USA']; ?></span><span><?= $model->getAuctionCostPrice() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getAuctionFee())): ?>
                                        <div>
                                            <span><?= $this->params['auction_cost']; ?></span><span><?= $model->getAuctionFee() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getBankCommission())): ?>
                                        <div>
                                            <span><?= $this->params['commission_bank'] ?></span><span><?= $model->getBankCommission() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getPowerOfAttorneyForForwarder())): ?>
                                        <div>
                                            <span><?= $this->params['power_of_accerdetor'] ?></span><span><?= $model->getPowerOfAttorneyForForwarder() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getEstimatedShippingCost())): ?>
                                        <div>
                                            <span><?= $this->params['estimate_shipping_cost'] ?></span><span><?= $model->getEstimatedShippingCost() ?></span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="popular__content">
                                    <?php if (!empty($model->getPortUnloading())): ?>
                                        <div>
                                            <span><?= $this->params['port_unloading'] ?></span><span><?= $model->getPortUnloading() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getCustomsBroker())): ?>
                                        <div>
                                            <span><?= $this->params['custom_broker'] ?></span><span><?= $model->getCustomsBroker() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getCustomsClearance())): ?>
                                        <div>
                                            <span><?= $this->params['custom_clearance'] ?></span><span><?= $model->getCustomsClearance() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getServices())): ?>
                                        <div>
                                            <span><?= $this->params['services'] ?></span><span><?= $model->getServices() ?></span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($model->getCostOfRepair())): ?>
                                        <div>
                                            <span><?= $this->params['cost_repair'] ?></span><span><?= $model->getCostOfRepair() ?></span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="popular__block">
                                <?php if (!empty($model->getTotal())): ?>
                                    <div>
                                        <span><?= $this->params['total'] ?></span><span><?= $model->getTotal() ?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($model->getEuro5Certificate())): ?>
                                    <div>
                                        <span><?= $this->params['certificate_5_euro'] ?></span><span><?= $model->getEuro5Certificate() ?></span>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($model->getAutoRegistrationAndPensionFund())): ?>
                                    <div>
                                        <span><?= $this->params['registration_auto'] ?></span><span><?= $model->getAutoRegistrationAndPensionFund() ?></span>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <div class="popular__info">
                            <h3 class="popular__title"><?= $model->getName($this->params['langId']) ?></h3>
                            <div class="popular__first">
                                <?php if (!empty($model->getPriceInEnjoyCars())): ?>
                                    <p><?= $model->getPriceInEnjoyCars() ?></p>
                                <?php endif; ?>
                                <?php if (!empty($model->getPriceInUkraine())): ?>
                                    <p><?= $model->getPriceInUkraine() ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="popular__second">
                                <?php if (!empty($model->getPriceInEnjoyCars())): ?>
                                    <p><?= $this->params['price_enjoy_cars'] ?></p>
                                <?php endif; ?>
                                <?php if (!empty($model->getPriceInUkraine())): ?>
                                    <p><?= $this->params['price_ukraine'] ?> <svg class="popular__svg" data-tag="symbol" id="svg-logo" viewBox="0 0 160 48"><title>AUTO.RIA - лідер автомобільної інтернет торгівлі</title><g><path fill-rule="evenodd" clip-rule="evenodd" fill="#DB5C4C" d="M0,0h92v48H11.9C5.4,48,0,42.6,0,36V0z"></path><g><path fill="#FFFFFF" d="M24,30.9L24,30.9c-0.3,0.2-0.6,0.5-0.9,0.7c-0.3,0.2-0.7,0.5-1.1,0.7c-0.4,0.2-0.8,0.4-1.3,0.5 c-0.5,0.1-1,0.2-1.6,0.2c-0.8,0-1.5-0.1-2.2-0.4c-0.7-0.2-1.2-0.6-1.7-1c-0.5-0.5-0.9-1-1.1-1.6c-0.3-0.6-0.4-1.4-0.4-2.1 c0-0.9,0.2-1.6,0.5-2.3c0.3-0.7,0.8-1.3,1.3-1.8c0.6-0.5,1.2-0.8,2-1.1c0.8-0.3,1.7-0.4,2.6-0.4c0.7,0,1.3,0,1.9,0.1 c0.6,0.1,1.1,0.2,1.6,0.3v-0.7c0-0.4-0.1-0.7-0.2-1.1c-0.1-0.3-0.3-0.7-0.6-0.9c-0.3-0.3-0.7-0.5-1.1-0.7c-0.5-0.2-1-0.2-1.7-0.2 c-0.8,0-1.6,0.1-2.4,0.3c-0.8,0.2-1.7,0.5-2.6,0.8v-3.1c0.8-0.4,1.7-0.6,2.6-0.8c0.9-0.2,1.9-0.3,2.9-0.3c1.2,0,2.2,0.1,3.1,0.4 c0.9,0.3,1.6,0.7,2.2,1.2c0.6,0.5,1,1.1,1.3,1.8c0.3,0.7,0.4,1.4,0.4,2.3v6.1c0,1.1,0,2,0,2.8c0,0.8,0,1.5,0.1,2.1h-3.6L24,30.9 L24,30.9z M23.7,25.8c-0.4-0.1-0.8-0.2-1.3-0.2c-0.5-0.1-1-0.1-1.5-0.1c-1,0-1.7,0.2-2.3,0.6c-0.6,0.4-0.8,1-0.8,1.8 c0,0.4,0.1,0.7,0.2,1c0.1,0.3,0.3,0.5,0.5,0.7c0.2,0.2,0.5,0.3,0.8,0.4c0.3,0.1,0.6,0.1,0.9,0.1c0.4,0,0.8-0.1,1.1-0.2 c0.4-0.1,0.7-0.2,1-0.4c0.3-0.2,0.6-0.3,0.8-0.5c0.2-0.2,0.4-0.4,0.6-0.6V25.8z"></path><path fill="#FFFFFF" d="M39.2,33.1c-1.6,0-3-0.3-4-0.8c-1.1-0.5-1.9-1.3-2.4-2.2c-0.3-0.5-0.5-1-0.6-1.7c-0.1-0.6-0.2-1.3-0.2-2 v-9.9h4v9.5c0,0.5,0,1,0.1,1.4c0.1,0.4,0.2,0.7,0.4,1c0.3,0.5,0.6,0.9,1.1,1.1c0.5,0.2,1.1,0.4,1.8,0.4c0.7,0,1.3-0.1,1.8-0.4 c0.5-0.3,0.9-0.7,1.1-1.2c0.3-0.5,0.4-1.3,0.4-2.2v-9.6h4v9.9c0,1.3-0.2,2.4-0.7,3.3c-0.3,0.5-0.6,1-1,1.4 c-0.4,0.4-0.9,0.8-1.4,1.1c-0.6,0.3-1.2,0.5-1.9,0.7C40.9,33,40.1,33.1,39.2,33.1z"></path><path fill="#FFFFFF" d="M60.6,32.6c-0.4,0.1-1,0.2-1.6,0.3c-0.6,0.1-1.2,0.1-1.7,0.1c-1.4,0-2.5-0.2-3.3-0.7 c-0.9-0.4-1.5-1.1-1.8-1.9c-0.3-0.6-0.4-1.4-0.4-2.4v-8.4h-3v-3.3h3v-4.5h4v4.5h4.7v3.3h-4.7v7.9c0,0.6,0.1,1.1,0.3,1.4 c0.3,0.5,1,0.8,2,0.8c0.5,0,0.9,0,1.4-0.1c0.5-0.1,0.9-0.2,1.3-0.3V32.6z"></path><path fill="#FFFFFF" d="M75.1,24.6c0-0.9-0.1-1.6-0.3-2.3c-0.2-0.7-0.5-1.2-0.8-1.7c-0.4-0.5-0.8-0.8-1.3-1 c-0.5-0.2-1-0.3-1.6-0.3c-0.6,0-1.1,0.1-1.6,0.3c-0.5,0.2-0.9,0.6-1.3,1c-0.4,0.4-0.6,1-0.8,1.7C67.1,23,67,23.7,67,24.6 c0,0.9,0.1,1.6,0.3,2.3c0.2,0.7,0.5,1.2,0.8,1.6c0.4,0.4,0.8,0.8,1.3,1c0.5,0.2,1,0.3,1.6,0.3c0.6,0,1.1-0.1,1.6-0.3 c0.5-0.2,0.9-0.6,1.3-1c0.4-0.4,0.6-1,0.8-1.6C75,26.2,75.1,25.4,75.1,24.6L75.1,24.6z M79.2,24.6c0,1.3-0.2,2.4-0.6,3.5 c-0.4,1-0.9,1.9-1.6,2.7c-0.7,0.7-1.6,1.3-2.6,1.7c-1,0.4-2.1,0.6-3.4,0.6c-1.3,0-2.4-0.2-3.4-0.6c-1-0.4-1.9-1-2.6-1.7 c-0.7-0.7-1.2-1.6-1.6-2.7c-0.4-1-0.6-2.2-0.6-3.5c0-1.3,0.2-2.4,0.6-3.5c0.4-1,0.9-1.9,1.6-2.7c0.7-0.7,1.6-1.3,2.6-1.7 c1-0.4,2.1-0.6,3.4-0.6c1.3,0,2.4,0.2,3.4,0.6c1,0.4,1.9,1,2.6,1.7c0.7,0.7,1.2,1.6,1.6,2.7C79,22.1,79.2,23.3,79.2,24.6z"></path></g><rect x="94" fill-rule="evenodd" clip-rule="evenodd" fill="#003B56" width="66" height="48"></rect><g><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M109.9,28c0.2,0.9,0.5,1.9,0.7,2.8c0,0,1.2-0.3,1.4-0.3 c0.4-0.1,0.7-0.2,1-0.4c0.7-0.5,0.3-1.4,0.3-2.1c-0.1-0.8-0.4-1.7-0.6-2.4c-0.1-0.4-0.5-1.2-1-1.2c-0.6,0-1.2,0.2-1.7,0.4 C108.9,25.1,109.8,27.3,109.9,28L109.9,28z M110.8,31.4c0.2,0.9,0.5,1.7,0.7,2.6c0.2,0.8,0.4,1.6,0.7,2.4c0.1,0.3,0.3,0.8,0.6,1 c0.3,0.2,0.8,0,1.2,0c0.4,0,0.4,0.4,0.1,0.5c-0.2,0.1-0.4,0.1-0.6,0.2c-2.3,0.6-4.6,1.2-7,1.8c-0.2,0-1.4,0.5-1.5,0.2 c-0.2-0.4,0.4-0.4,0.6-0.6c0.4-0.2,0.6-0.5,0.6-0.9c0-0.8-0.3-1.7-0.5-2.5c-0.2-0.7-0.3-1.5-0.5-2.2c-0.5-1.8-0.9-3.6-1.5-5.4 c-0.2-0.7-0.4-1.9-1.3-1.8c-0.3,0-0.8,0.3-0.9-0.1c-0.1-0.2,0.1-0.4,0.3-0.5c2.1-0.5,4.2-1.1,6.2-1.6c1.6-0.4,3.3-0.9,4.9-1.3 c0.8-0.2,1.6-0.3,2.5-0.4c0.9,0,1.7-0.2,2.4,0.4c1.2,0.9,1.7,3.2,0.8,4.4c-0.5,0.7-1.3,1.1-2,1.5c-0.7,0.4-1.5,0.6-2.3,0.9 c-0.5,0.2-1.1,0.3-1.7,0.5c0.8,0.3,1.6,0.5,2.4,0.4c0.8-0.1,1.6-0.5,2.3-0.8c0.2-0.1,0.8-0.4,1-0.3c0.3,0.1,0.4,0.7,0.5,1 c0.3,0.7,0.9,1.4,1.4,2c0.5,0.6,1.2,1.2,1.8,1.6c0.3,0.2,0.7,0.4,1.1,0.4c0.2,0,0.8-0.1,0.6,0.3c-0.1,0.2-0.4,0.2-0.6,0.2 c-0.4,0.1-0.9,0.2-1.3,0.3c-1.3,0.3-2.6,0.7-3.8,1c-0.4,0.1-0.8,0.2-1.2,0.3c-0.2,0-0.5,0.2-0.6,0.1c-0.2-0.1-0.3-0.4-0.4-0.6 c-0.2-0.3-0.5-0.7-0.7-1c-0.8-1.1-1.5-2.2-2.3-3.2c-0.3-0.4-0.5-0.7-0.8-1.1c-0.2-0.3-0.3-0.2-0.7-0.1 C111.3,31.3,111,31.3,110.8,31.4L110.8,31.4z M126,28.8c0.2,0.8,0.4,1.6,0.6,2.5c0.1,0.4,0.2,0.8,0.2,1.2c0.1,0.4,0.2,0.9,0,1.2 c-0.2,0.4-0.7,0.4-1,0.6c-0.2,0.1-0.5,0.6-0.1,0.5c0.6-0.1,1.3-0.3,1.9-0.5c2.3-0.6,4.6-1.2,6.9-1.8c0.2,0,0.4-0.1,0.6-0.2 c0.4-0.3-0.3-0.4-0.5-0.4c-0.3,0-1,0.2-1.1-0.1c-0.3-0.3-0.4-0.6-0.6-1c-0.3-0.7-0.5-1.6-0.7-2.4c-0.2-0.9-0.4-1.7-0.7-2.6 c-0.4-1.7-0.8-3.3-1.2-5c-0.1-0.4-0.2-0.9,0-1.2c0.2-0.4,0.6-0.5,0.9-0.6c0.3-0.1,0.7-0.5,0.2-0.6c-0.2-0.1-0.4,0-0.6,0.1 c-2.3,0.6-4.6,1.2-6.9,1.8c-0.6,0.2-1.4,0.2-1.9,0.5c-0.4,0.2,0.1,0.5,0.3,0.5c0.4,0,0.8-0.2,1.2,0c0.3,0.2,0.5,0.7,0.6,1.1 c0.1,0.4,0.2,0.7,0.3,1.1c0.2,0.8,0.5,1.6,0.7,2.4C125.5,27,125.7,27.9,126,28.8L126,28.8L126,28.8z M142.9,24.2 c-1-1.4-2-2.7-2.9-4.1c0.1,0.1,0,0.6,0,0.7c0,0.6-0.1,1.2-0.1,1.7c-0.1,0.9-0.1,1.7-0.2,2.6C140.7,24.8,141.8,24.5,142.9,24.2 L142.9,24.2z M139.6,25.7c0,0.8-0.1,1.9,0.4,2.6c0.2,0.3,0.5,0.5,0.7,0.7c0.3,0.3,0.6,0.6,1,0.7c0.4,0.1,0.8,0.1,1.1,0.2 c0.4,0.2-0.1,0.4-0.3,0.5c-0.8,0.2-1.7,0.4-2.5,0.7c-1.2,0.3-2.3,0.6-3.5,0.9c-0.2,0.1-0.5,0.1-0.7,0.1c-0.5,0,0-0.4,0.1-0.5 c1.4-0.8,1.9-2.8,2.3-4.3c0.5-1.7,0.5-3.4,0.7-5.2c0.1-0.9,0.2-1.7,0.2-2.6c0-0.9-1.2-0.7-1.8-0.9c-0.4-0.1-0.4-0.3,0-0.5 c0.4-0.2,0.8-0.2,1.2-0.4c0.8-0.3,1.5-0.8,2.1-1.3c0.7-0.5,1.2-1.1,1.8-1.7c0.2-0.2,0.4-0.5,0.6-0.2c0.3,0.3,0.5,0.6,0.7,0.9 c2.1,2.9,4.2,5.8,6.4,8.7c0.5,0.6,0.9,1.3,1.4,1.8c0.3,0.3,0.5,0.6,0.8,0.8c0.3,0.3,0.6,0.3,1,0.2c0.2,0,0.3,0,0.5,0.1 c0.1,0,0.3,0.1,0.2,0.2c-0.1,0.1-0.3,0.2-0.5,0.2c-0.9,0.2-1.8,0.5-2.7,0.7c-2.1,0.6-4.2,1.1-6.3,1.7c-0.1,0-0.3,0.1-0.4,0.1 c-0.1,0-0.2-0.2-0.2-0.3c0-0.2,0.3-0.2,0.4-0.3c0.4-0.2,0.8-0.4,0.9-0.8c0.2-0.4,0-0.8-0.2-1.2c-0.2-0.5-0.5-1-0.9-1.4 c-0.2-0.2-0.3-0.4-0.5-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c0-0.1-0.2-0.3-0.3-0.3c-0.1,0-0.4,0.1-0.5,0.1c-0.4,0.1-0.8,0.2-1.3,0.3 C140.8,25.4,140.2,25.5,139.6,25.7L139.6,25.7z"></path></g><g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="120.3,14.8 113.8,13.9 120.9,12.1 122.2,7.2 126.5,10.9 139.4,8 129.3,13.6 132.9,16.7 125.4,15.6 118.6,19.8"></polygon></g></g></svg></p>
                                <?php endif; ?>
                            </div>
                            <a class="popular__callback js-popup-madel" href=""><?= $this->params['get_want_1'] ?></a>
                        </div>
                    </div>
                    <?php if ($key % 4 == 3) : ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section id="car-select" class="car-select">
        <div class="car-select__container">
            <div class="car-select__inner">
                <h2 class="car-select__caption"><?= $this->params['what_car_you_want'] ?></h2>
                <form class="form-top-quizz" action="/callback" method="post" id="form-top-quizz">

                    <div class="field-wrap">
                        <select class="field-select" name="model_type_id">
                            <option value="def" selected="selected" disabled="disabled"><?= $this->params['type_of_car'] ?></option>
                            <?php foreach ($modelTypes as $modelType): ?>
                                <option value="<?= $modelType->getId();?>"><?= $modelType->getName($this->params['langId']);?></option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <!-- <div class="field-wrap">
                        <select class="field-select" name="year_of_issue_from">
                            <option value="def" selected="selected" disabled="disabled"><?= $this->params['issue_from_year'] ?></option>
                            <?php for ($i = 2010; $i <= date('Y'); $i++): ?>
                                <option value="<?= $i;?>"><?= $i;?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="field-wrap">
                        <select class="field-select" name="year_of_issue_to">
                            <option value="def" selected="selected" disabled="disabled"><?= $this->params['issue_to_year'] ?></option>
                            <?php for ($i = 2010; $i <= date('Y'); $i++): ?>
                                <option value="<?= $i;?>"><?= $i;?></option>
                            <?php endfor; ?>
                        </select>
                    </div> -->
                    
                    <div class="field-wrap">
                        <select class="field-select" name="turnkey_budget" id="turnkey_budget">
                            <option value="def" selected="selected" disabled="disabled"><?= $this->params['budget_by_key'] ?></option>
                            <?php foreach (\app\models\CalculatorConst::BUDGETS as $budget):?>
                                <option data-discount="<?= $this->params[$budget['discountAlias']] ?>" value="<?= $this->params[$budget['budgetAlias']];?>"><?= $this->params[$budget['budgetAlias']];?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="field-wrap">
                        <input class="field-input phone-mask clear" type="tel" name="phone" required="" autocomplete="off" placeholder="Телефон" minlength="17" maxlength="17">
                    </div>

                    <div class="field-wrap">
                        <div class="car-select__text" style="display: none"><?= $this->params['vigognee_ot_rynka_ukraine']?> <span class="price" id="car_select_discount"></span></div>
                    </div>
                    

                    <!-- <p class="car-select__notification"><?= $this->params['message_sent'] ?></p> -->
                    <button id="btn-top-form" type="submit" class="button button-block"><?= $this->params['get_the_auto'] ?></button>
                </form>
            </div>
        </div>
    </section>
    <?php if (!empty($reviews)):?>
        <section id="reviews" class="reviews">
            <div class="reviews__container">
                <h2 class="reviews__caption"><?= $this->params['reviews_our_clients'];?></h2>
                <div class="reviews__slider owl-carousel owl-theme">
                    <?php foreach ($reviews as $review): ?>
                        <div class="reviews__slide item">
                            <div class="review__card">
                                <img class="review__img" src="<?= $review->getImg()?>" alt="<?= $review->getName($this->params['langId']);?>">
                                <div class="review__content">
                                    <div class="review__name"><?= $review->getName($this->params['langId']);?></div>
                                    <div class="review__text review__text--closed"><?= $review->getDescription($this->params['langId']);?></div>
                                    <span class="review__more js-review-more" data-text-detail="Подробнее" data-text-closed="Свернуть">Подробнее</span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </section>
    <?php endif;?>
    <section id="stage" class="stage">
        <div class="stage__container">
            <h2 class="stage__caption"><?= $this->params['stage_of_auto'] ?></h2>
            <div class="stage__blocks adaptive--hide">
                <?php foreach ($stages as $stage): ?>
                    <div class="stage__item revealator-slideup revealator-once">
                        <img class="stage__img" src="<?= $stage->getImg() ?>"
                             alt="<?= $stage->getName($this->params['langId']) ?>">
                        <h3 class="stage__title"><?= $stage->getName($this->params['langId']) ?></h3>
                        <p class="stage__desc"><?= $stage->getDescription($this->params['langId']) ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="stage__slider adaptive--show owl-carousel owl-theme">
                <?php foreach ($stages as $stage): ?>
                    <div class="stage__item item">
                        <div class="stage__item revealator-slideup revealator-once">
                            <img class="stage__img" src="<?= $stage->getImg() ?>"
                                 alt="<?= $stage->getName($this->params['langId']) ?>">
                            <h3 class="stage__title"><?= $stage->getName($this->params['langId']) ?></h3>
                            <p class="stage__desc"><?= $stage->getDescription($this->params['langId']) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php foreach ($quotations as $quotation): ?>
        <section class="quotation" style="background: url(<?= $quotation->getBackground() ?>) no-repeat center / cover">
            <div class="quotation__container">
                <div class="quotation__block">
                    <h2 class="quotation__caption"><?= $quotation->getName($this->params['langId']) ?></h2>
                    <a class="main-block__callback js-popup" href="#"><?= $this->params['get_call_2'] ?></a>
                </div>
                <img class="quotation__img revealator-slideleft revealator-delay2 revealator-once"
                     src="<?= $quotation->getCatImg() ?>" alt="<?= $quotation->getName($this->params['langId']) ?>">
            </div>
        </section>
    <?php endforeach; ?>
    <section id="services" class="services">
        <div class="services__container">
            <h2 class="services__caption"><?= $this->params['packages_services'] ?></h2>
            <p class="services__desc"><?= $this->params['packages_services_additional'] ?></p>
            <div class="services__blocks">
                <?php foreach ($services as $service): ?>
                    <div class="services__item">
                        <h3 class="services__title"><?= $service->getName($this->params['langId']); ?></h3>
                        <p class="services__list">
                           <?= $service->getList($this->params['langId']);?>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>
            <div id="counter" class="services__counter">
                <?php foreach ($countServices as $countService): ?>
                    <div class="services__numeral">
                        <span class="services__number"><?= $countService->getNumber() ?></span><span><?= $countService->getSign() ?></span>
                        <p><?= $countService->getName($this->params['langId']) ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section id="team" class="team">
        <div class="team__container">
            <h2 class="team__caption"><?= $this->params['team'] ?></h2>
            <div class="team__slider owl-carousel owl-theme">
                <?php foreach ($teams as $team): ?>
                    <div class="team__item item">
                        <img class="team__img" src="<?= $team->getImg() ?>"
                             alt="<?= $team->getName($this->params['langId']) ?>">
                        <h3 class="team__title"><?= $team->getName($this->params['langId']) ?></h3>
                        <p class="team__desc"><?= $team->getSpecialization($this->params['langId']) ?></p>
                        <a href="#" class="team__more js-popup-team"><?= $this->params['more'] ?></a>
                        <a class="team__link"
                           href="<?= $team->getLink() ?>"><?= $this->params['link_to_telegram'] ?></a>
                        <div class="team__hide">
                            <h3 class="team__title"><?= $team->getName($this->params['langId']) ?></h3><img
                                    class="team__close" src="/img/x.svg" alt="">
                            <p class="team__desc"><?= $team->getSpecialization($this->params['langId']) ?></p>
                            <div class="team__info">
                                <p class="team__text"><?= $team->getDescription($this->params['langId']) ?></p>
                                <a class="team__link" href=""<?= $team->getLink() ?>
                                "><?= $this->params['link_to_telegram'] ?></a>
                            </div>
                        </div>
                        <div class="team__overlay"></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section id="contacts" class="contacts">
        <div class="contacts__container">
            <div class="contacts__left">
                <h2 class="contacts__caption"><?= $this->params['our_contacts'] ?></h2>
                <div class="contacts__phone">
                    <h3 class="contacts__title"><?= $this->params['phone'] ?></h3>
                    <a class="contacts__info" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['first_phone']) ?>"><?= $this->params['first_phone'] ?></a><br>
                    <a class="contacts__info" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['second_phone']) ?>"><?= $this->params['second_phone'] ?></a><br>
                    <a class="contacts__info binct-phone-number-1" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['third_phone']) ?>"><?= $this->params['third_phone'] ?></a><br>
                    <a class="contacts__info binct-phone-number-2" href="tel:<?= str_replace([' ', '+', '(', ')'], '',
                        $this->params['fourth_phone']) ?>"><?= $this->params['fourth_phone'] ?></a>
                </div>
                <div class="contacts__email">
                    <h3 class="contacts__title"><?= $this->params['email'] ?></h3>
                    <a class="contacts__info"
                       href="mailto:<?= $this->params['enjoy_email'] ?>"><?= $this->params['enjoy_email'] ?></a>
                </div>
                <div class="contacts__address">
                    <h3 class="contacts__title"><?= $this->params['address'] ?></h3>
                    <p class="contacts__info"><?= $this->params['our_address'] ?></p>
                </div>
                <a href="#" class="contacts__link js-popup"><?= $this->params['get_call'] ?></a>
            </div>
            <div class="contacts__right">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d448.9977866183778!2d30.508083317555027!3d50.46130680526588!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0:0x30e399cc5aa996cb!2sEnjoy Cars!5e0!3m2!1sru!2sua!4v1570704505967!5m2!1sru!2sua" allowfullscreen=""></iframe>
            </div>
        </div>
    </section>
    <div class="popup">
        <h2 class="popup__caption"><?= $this->params['get_call'] ?></h2><img class="popup__close" src="/img/x.svg"
                                                                             alt="">
        <form class="popup__form" action="#">
<!--            <div class="popup__field">-->
<!--                <input class="popup__name clear" type="text" name="name" placeholder="--><?//= $this->params['placeholder_name'] ?><!--" pattern="[А-я]{3,}"-->
<!--                       required>-->
<!--                <span class="popup__error">--><?//= $this->params['validate_name'] ?><!--</span>-->
<!--            </div>-->
            <div class="popup__field">
                <input class="popup__phone phone-mask clear" type="tel" name="phone" placeholder="<?= $this->params['placeholder_phone'] ?>"
                       minlength="17" maxlength="17"
                       required>
                <span class="popup__error"><?= $this->params['validate_phone'] ?></span>
            </div>
            <input class="popup__send" id="popupFormSubmit" type="submit" value="Оставить заявку">
        </form>
        <p class="popup__notification"><?= $this->params['message_sent'] ?></p>
    </div>
    <div class="popup-model">
        <h2 class="popup-model__caption"><?= $this->params['get_call'] ?></h2><img class="popup-model__close"
                                                                                   src="/img/x.svg" alt="">
        <form class="popup-model__form" action="#">
            <div class="popup-model__field">
                <input class="popup-model__model" type="text" name="model" required disabled value="">
            </div>
            <div class="popup-model__field">
                <input class="popup-model__name clear" type="text" name="name" placeholder="<?= $this->params['placeholder_name'] ?>" pattern="[А-я]{3,}"
                       required>
                <span class="popup-model__error"><?= $this->params['validate_name'] ?></span>
            </div>
            <div class="popup-model__field">
                <input class="popup-model__phone phone-mask clear" type="tel" name="phone" placeholder="<?= $this->params['placeholder_phone'] ?>"
                       minlength="17" maxlength="17"
                       required>
                <span class="popup-model__error"><?= $this->params['validate_phone'] ?></span>
            </div>
            <input class="popup-model__send" id="popupModelSubmit" type="submit" value="Оставить заявку">
        </form>
        <p class="popup-model__notification"><?= $this->params['message_sent'] ?></p>
    </div>
    <div class="popup__overlay"></div>
</main>
