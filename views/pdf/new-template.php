<?php
/**
 * @var \app\models\PdfParams $pdfParams
 */
?>

<table style="border-collapse:collapse;border-bottom: 2px solid #FFBF00;" width="100%">
    <tr>
        <td align="left" width="30%"
            style="color: #FFBF00;font-size: 26px;font-weight: bold;padding-bottom: 15px;letter-spacing: .5px;">
            <img width="100px" src="/img/favicon.png" alt="">
        </td>
        <td align="right" width="70%"
            style="color: #000;font-size: 20px;font-weight: bold;padding-bottom: 15px;letter-spacing: .5px;">Калькуляция по автомобилю<br><br>
            <a style="font-size: 14px;" href="tel:0443346715">+380443346715</a>
            <br>
            <a style="font-size: 14px;" href="https://enjoycars.com.ua/">https://enjoycars.com.ua/</a>
        </td>
    </tr>
</table>
<br/>
<?php $amount = 0;?>
<table width="100%">
    <tr bgcolor="#e9e9e9">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;text-align:center;font-weight: bold;font-size: 14px;">
            &#x2116;
        </td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;text-align:center;font-weight: bold;font-size: 14px;">
            Этапы от Enjoy Cars
        </td>
        <td width="8%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;text-align:center;font-weight: bold;font-size: 14px;">
            Стоимость USD
        </td>
        <td width="8%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;text-align:center;font-weight: bold;font-size: 14px;">
            Сроки и шаги поэтапной оплаты
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">1</td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Стоимость авто на аукционе</td>
        <?php $amount += $pdfParams->getPrice();?>
        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getPrice() ?>
            $
        </td>
        <td rowspan="2" width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">
            Оплата в течении 3 дней после покупки авто
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">2</td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Комиссия аукциона
        </td>
        <?php $amount += $pdfParams->getAuctionFee();?>
        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getAuctionFee() ?> $
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">3</td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Доставка в Украину <br><span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(доставка по США, погрузка на судно, доставка морем в порт Одесса, фотоотчет)</span></td>
        <?php $amount += $pdfParams->getDelivery();?>

        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><del><?= ($pdfParams->getDelivery() + 160)?> $</del><br><?= $pdfParams->getDelivery() ?> $
        </td>
        <td width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Оплата после загрузки авто в контейнер</span>
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">4</td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">
            Комплекс (Порт Одесса) <span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(выгрузка, экспедирование, брокер, фотоотчет)</span>
        </td>
        <?php $amount += $pdfParams->getFreightForwarding();?>

        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getFreightForwarding() ?>
            $
        </td>
        <td width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Оплата перед разгрузкой контейнера в порту Одесса
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">5</td>
        <td width="30%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Таможенная очистка <span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(акциз+пошлина+НДС)</span>
        </td>
        <?php $amount += $pdfParams->getCustomsClearance();?>

        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getCustomsClearance() ?>
            $
        </td>
        <td width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">
            Оплата после прибытия автомобиля в порт Одесса
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%"
            style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">6</td>
        <td width="30%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Страховка  <br><span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(от вторичных повреждений)</span></td>
        <?php $amount += $pdfParams->getInsurance();?>
        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getInsurance() ?>
            $
        </td>
        <td width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Оплата в течении 5 дней после успешных торгов
        </td>
    </tr>
    <tr style="line-height:15px;">
        <td width="7%" style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">7</td>
        <td width="30%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Услуги Enjoy Cars <br><span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(согласно договору)</span></td>
        <?php $amount += $pdfParams->getService();?>
        <td width="15%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getService() ?>
            $
        </td>
        <td width="45%"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">
            Оплата в течении 5 дней после успешных торгов
        </td>
    </tr>
    <tr style="font-weight:bold;line-height:20px;">
        <td colspan="1">&nbsp;</td>
        <td bgcolor="#e9e9e9"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-weight: bold;font-size: 16px;">total $
        </td>
        <td bgcolor="#e9e9e9"
            style="padding-left:4px;color: #000000;border:1px solid #494949;font-weight: bold;font-size: 16px;"><?= $amount?> $</td>
    </tr>
</table>

<table style="border-collapse:collapse;border:none;margin-top: 30px" width="100%">
    <tr>
        <td width="54%">

            <table width="100%">
			    <tr bgcolor="#e9e9e9">
			        <td colspan="3" width="100%" style="color: #000000;border:1px solid #494949;text-align:center;font-weight: bold;font-size: 14px;">
			            Дополнительные услуги
			        </td>
			    </tr>
                <tr style="line-height:15px;">
                	<td width="12%" style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">1</td>
                    <td width="63%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Доставка по Украине+Экспедирование <span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(доставка на СТО,сертификацию,РСЦ экспедитором Enjoy Cars)</td>
                    <td width="28%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getDeliveryToCityCost() ?>  $</td>
                </tr>
                <tr style="line-height:15px;">
                	<td width="12%" style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">2</td>
                    <td width="60%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Минимальная стоимость ремонта</td>
                    <td width="28%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getRepair() ?>  $</td>
                </tr>
                <tr style="line-height:15px;">
                	<td width="12%" style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">3</td>
                    <td width="63%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Сертификат соответствия ТС</td>
                    <td width="28%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= $pdfParams->getCertification() ?> $</td>
                </tr>
                <tr style="line-height:15px;">
                	<td width="12%" style="color: #000000;border:1px solid #494949;font-size: 14px;text-align:center;">4</td>
                    <td width="60%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;">Первичная регистрация в РСЦ <span style="display:inline-block; color: #000000;font-size: 10px;text-align:left;">(постановка авто на украинский учет без присутствия в день сертификации)</td>
                    <td width="28%" style="padding-left:4px;color: #000000;border:1px solid #494949;font-size: 14px;"><?= ($pdfParams->getRegistration()+100.0) ?>  $</td>
                </tr>
            </table>

        <td width="46%" align="right" style="vertical-align: top; text-align: right;">
            <img width="250px" src="<?= $pdfParams->getAuctionLogo(); ?>" alt="logo">
        </td>
        </td>
    </tr>
</table>