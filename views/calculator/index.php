<?php

/** @var $this \yii\web\View
 * @var $ports \app\models\Port[]
 * @var $auctions \app\models\Auction[]
 * @var $platforms \app\models\Platform[]
 **/
?>

<section class="calculator">
    <div class="calculator__container">
        <h1 class="calculator__caption">Калькулятор</h1>
        <form action="#" class="calculator__form" onsubmit="sConsole(event)">
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__buy">Покупка</label>
                <input id="calculator__buy" class="calculator__buy" name="price" type="number" step="0.1" min="0"
                       placeholder="0,0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__volume">Обьем</label>
                <input id="calculator__volume" class="calculator__volume" name="engineVolume" type="number" step="0.1"
                       min="0" placeholder="0,0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__year">Год выпуска</label>
                <select id="calculator__year" class="calculator__year" name="year" required>
                    <?php for ($i = ((int)date('Y') - 1); $i >= 2000; $i--): ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>

                </select>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__auction">Аукцион</label>
                <select id="calculator__auction" class="calculator__auction" name="auctionId" required>
                    <?php foreach ($auctions as $auction): ?>
                        <option value="<?= $auction->getId() ?>"><?= $auction->getName() ?></option>
                    <?php endforeach; ?>

                </select>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__platform">Площадка</label>
                <select id="calculator__platform" class="calculator__platform" name="platformId" required>
                    <?php foreach ($platforms as $platform): ?>
                        <option data-type="<?= $platform->getAuctionId() ?>"
                                value="<?= $platform->getId() ?>"><?= $platform->getName() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__waterfront">Порт</label>
                <select id="calculator__waterfront" class="calculator__waterfront" name="portId" required>
                    <?php foreach ($ports as $port): ?>
                        <option value="<?= $port->getId() ?>"><?= $port->getName() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__service">Услуга</label>
                <input id="calculator__service" class="calculator__service" name="service" type="number" step="0.1"
                       min="0" placeholder="0,0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__repair">Ремонт</label>
                <input id="calculator__repair" class="calculator__repair" name="repair" type="number" step="1" min="0"
                       placeholder="0,0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__insurance">Страховка</label>
                <input id="calculator__insurance" class="calculator__insurance" name="insurance" type="number" step="1" min="0"
                       placeholder="0,0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__motor">Тип двигателя</label>
                <select id="calculator__motor" class="calculator__motor" name="engineTypeId" required>
                    <option value="1">Бензин</option>
                    <option value="2">Дизель</option>
                </select>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__delivery">Доставка в ваш город</label>
                <input id="calculator__delivery" class="calculator__delivery" name="deliveryToCity" type="number" step="5" min="0"
                       placeholder="0" required>
            </div>
            <div class="calculator__block">
                <label class="calculator__label" for="calculator__img">Логотип аукциона</label>
                <input id="calculator__img" class="calculator__img" type="text" name="auctionLogo" required>
            </div>
            <input class="calculator__submit" type="submit" value="Просчитать">
        </form>
        <a style="display: none" class="pdf_link" target="_blank">PDF</a>
    </div>
</section>

