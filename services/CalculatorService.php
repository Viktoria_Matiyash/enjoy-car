<?php

namespace app\services;

use app\models\Auction;
use app\models\CalculatorConst;
use app\models\PdfParams;
use app\models\Platform;
use app\models\Port;
use app\repositories\AuctionRepository;
use app\repositories\CalculatorExtraPriceRepository;
use app\repositories\CallbackRepository;
use app\repositories\PdfParamsRepository;
use app\repositories\PdfRepository;
use app\repositories\PlatformRepository;
use app\repositories\PortRepository;
use kartik\mpdf\Pdf;
use yii\base\View;
use yii\web\Request;

class CalculatorService
{
    /**
     * @var CallbackRepository
     */
    protected $portRepository;

    /**
     * @var AuctionRepository
     */
    protected $auctionRepository;

    /**
     * @var CalculatorExtraPriceRepository
     */
    protected $calculatorExtraPriceRepository;

    /**
     * @var PdfParamsRepository
     */
    protected $pdfParamsRepository;

    /**
     * @var PdfRepository
     */
    protected $pdfRepository;

    /**
     * @var PlatformRepository
     */
    protected $platformRepository;

    /**
     * @var Pdf
     */
    protected $pdf;

    /**
     * @var View
     */
    protected $view;

    /**
     * CalculatorService constructor.
     * @param PortRepository $portRepository
     * @param AuctionRepository $auctionRepository
     * @param CalculatorExtraPriceRepository $calculatorExtraPriceRepository
     * @param PdfParamsRepository $pdfParamsRepository
     * @param PdfRepository $pdfRepository
     * @param PlatformRepository $platformRepository
     * @param Pdf $pdf
     * @param View $view
     */
    public function __construct(
        PortRepository $portRepository,
        AuctionRepository $auctionRepository,
        CalculatorExtraPriceRepository $calculatorExtraPriceRepository,
        PdfParamsRepository $pdfParamsRepository,
        PdfRepository $pdfRepository,
        PlatformRepository $platformRepository,
        Pdf $pdf,
        View $view
    ) {
        $this->portRepository = $portRepository;
        $this->auctionRepository = $auctionRepository;
        $this->calculatorExtraPriceRepository = $calculatorExtraPriceRepository;
        $this->pdfParamsRepository = $pdfParamsRepository;
        $this->pdfRepository = $pdfRepository;
        $this->platformRepository = $platformRepository;
        $this->pdf = $pdf;
        $this->view = $view;
    }

    /**
     * @return Port[]
     */
    public function findPorts(): array
    {
        return $this->portRepository->findAll();
    }

    /**
     * @return Auction[]
     */
    public function findAuctions(): array
    {
        return $this->auctionRepository->findAll();
    }

    /**
     * @return Platform[]
     */
    public function findPlatforms(): array
    {
        return $this->platformRepository->findAll();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function generatePDF(Request $request)
    {
        return $this->createPdf($this->preparePdfParams($request->getBodyParams()));
    }

    /**
     * @param array $params
     * @return PdfParams
     */
    private function preparePdfParams(array $params): PdfParams
    {
        $auctionId = (int)$params['auctionId'];
        $price = (float)$params['price'];
        $engineTypeId = (int)$params['engineTypeId'];
        $year = (int)$params['year'];
        $engineVolume = (float)$params['engineVolume'];
        $portId = (int)$params['portId'];
        $platformId = (int)$params['platformId'];
        $deliveryToCityCost = (float)$params['deliveryToCity'];
        $insurance = (float)$params['insurance'];
        $auctionLogo = (string)$params['auctionLogo'];
        $platform = $this->platformRepository->findById($platformId);
        if ($platform->getDeliveryCost($portId) == 0) {
            throw new \DomainException('Dont have price to this port, try another port');
        }
        $buyerFee = $this->calculatorExtraPriceRepository->findByParams(CalculatorExtraPriceRepository::BUYER_FEE_TYPE,
            $auctionId, $price);
        $internetFee = $this->calculatorExtraPriceRepository->findByParams(CalculatorExtraPriceRepository::INTERNET_FEE_TYPE,
            $auctionId, $price);
        $auction = $this->auctionRepository->findById($auctionId);
        $auctionFee = $buyerFee->getCommission() + $internetFee->getCommission() + $auction->getCostService() + ($price * $buyerFee->getPercentFromCost()) + CalculatorConst::AUCTION_CONST;
        $customsValue = $auctionFee + $price + CalculatorConst::CUSTOMS_VALUE;
        $duty = $customsValue * CalculatorConst::DUTY;
        $coefficientYear = ($year === 2019) ? 1 : date('Y') - $year - 1;
        $baseRate = 0;
        if ($engineTypeId == CalculatorConst::PETROL_ID) {
            if ($engineVolume > CalculatorConst::PETROL_COEFFICIENT) {
                $baseRate = CalculatorConst::PETROL_LARGE_PRICE;
            } else {
                $baseRate = CalculatorConst::PETROL_LOW_PRICE;
            }
        }

        if ($engineTypeId == CalculatorConst::DIESEL_ID) {
            if ($engineVolume > CalculatorConst::DIESEL_COEFFICIENT) {
                $baseRate = CalculatorConst::DIESEL_LARGE_PRICE;
            } else {
                $baseRate = CalculatorConst::DIESEL_LOW_PRICE;
            }
        }
        $excise = $baseRate * $engineVolume * $coefficientYear;
        $vat = ($customsValue + $excise + $duty) * CalculatorConst::VAT;
        $customsClearance = $duty + $excise + $vat + CalculatorConst::CUSTOMS_CLEARANCE_PRICE;
        $pensionFund = ($price + $auctionFee) * CalculatorConst::PENSION_FUND_COEFFICIENT;
        $registration = $pensionFund + CalculatorConst::REGISTRATION_PRICE;
        $port = $this->portRepository->findById($portId);
        $delivery = $port->getDeliveryCost() + $platform->getDeliveryCost($portId);
        $pdfParams = $this->pdfParamsRepository->createPdfParams([
            'price' => $price,
            'auctionFee' => $auctionFee,
            'delivery' => $delivery,
            'freightForwarding' => CalculatorConst::BROKERS,
            'service' => (float)$params['service'],
            'repair' => (float)$params['repair'],
            'certification' => CalculatorConst::CERTIFICATION,
            'registration' => $registration,
            'customsClearance' => $customsClearance,
            'duty' => $duty,
            'customValue' => $customsValue,
            'excise' => $excise,
            'vat' => $vat,
            'customClearance' => $customsClearance,
            'deliveryToCityCost' => $deliveryToCityCost,
            'auctionLogo' => $auctionLogo,
            'insurance' => $insurance,
        ]);

        return $pdfParams;
    }

    /**
     * @param PdfParams $pdfParams
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    private function createPdf(PdfParams $pdfParams)
    {
        $pdfBody = $this->view->render('@pdf/new-template', [
            'pdfParams' => $pdfParams
        ]);
        $fileName = date('U') . '.pdf';

        $file = \Yii::getAlias('@userfiles') . '/' . $fileName;
        $this->pdf->mode = Pdf::MODE_UTF8;
        $this->pdf->format = Pdf::FORMAT_A4;
        $this->pdf->orientation = Pdf::ORIENT_PORTRAIT;
        $this->pdf->destination = Pdf::DEST_FILE;
        $this->pdf->filename = $file;
        $this->pdf->content = $pdfBody;
        $this->pdf->options = ['subject' => 'PDF'];
        $this->pdf->methods = ['SetTitle' => $fileName];
        $this->pdf->render();
        $pdfModel = $this->pdfRepository->createPdf(['name' => $fileName, 'file' => $fileName]);
        $this->pdfRepository->save($pdfModel);

        return $pdfModel;
    }
}