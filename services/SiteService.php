<?php

namespace app\services;

use app\models\Advantage;
use app\models\Menu;
use app\models\Model;
use app\models\ModelType;
use app\models\Quotation;
use app\models\Review;
use app\models\Service;
use app\models\ServiceCounter;
use app\models\Slider;
use app\models\Stage;
use app\models\Team;
use app\repositories\AdvantageRepository;
use app\repositories\MenuRepository;
use app\repositories\ModelRepository;
use app\repositories\ModelTypeRepository;
use app\repositories\QuotationRepository;
use app\repositories\ReviewRepository;
use app\repositories\ServiceCounterRepository;
use app\repositories\ServiceRepository;
use app\repositories\SliderRepository;
use app\repositories\StageRepository;
use app\repositories\TeamRepository;

class SiteService
{

    /**
     * @var AdvantageRepository
     */
    protected $advantageRepository;

    /**
     * @var ModelRepository
     */
    protected $modelRepository;

    /**
     * @var QuotationRepository
     */
    protected $quotationRepository;

    /**
     * @var ServiceCounterRepository
     */
    protected $serviceCounterRepository;

    /**
     * @var ServiceRepository
     */
    protected $serviceRepository;

    /**
     * @var SliderRepository
     */
    protected $sliderRepository;

    /**
     * @var StageRepository
     */
    protected $stageRepository;

    /**
     * @var TeamRepository
     */
    protected $teamRepository;

    /**
     * @var MenuRepository
     */
    protected $menuRepository;

    /**
     * @var ReviewRepository
     */
    protected $reviewRepository;

    /**
     * @var ModelTypeRepository
     */
    protected $modelTypeRepository;

    /**
     * SiteService constructor.
     * @param AdvantageRepository $advantageRepository
     * @param ModelRepository $modelRepository
     * @param QuotationRepository $quotationRepository
     * @param ServiceCounterRepository $serviceCounterRepository
     * @param ServiceRepository $serviceRepository
     * @param SliderRepository $sliderRepository
     * @param StageRepository $stageRepository
     * @param TeamRepository $teamRepository
     * @param MenuRepository $menuRepository
     * @param ReviewRepository $reviewRepository
     * @param ModelTypeRepository $modelTypeRepository
     */
    public function __construct(
        AdvantageRepository $advantageRepository,
        ModelRepository $modelRepository,
        QuotationRepository $quotationRepository,
        ServiceCounterRepository $serviceCounterRepository,
        ServiceRepository $serviceRepository,
        SliderRepository $sliderRepository,
        StageRepository $stageRepository,
        TeamRepository $teamRepository,
        MenuRepository $menuRepository,
        ReviewRepository $reviewRepository,
        ModelTypeRepository $modelTypeRepository
    ) {
        $this->advantageRepository = $advantageRepository;
        $this->modelRepository = $modelRepository;
        $this->quotationRepository = $quotationRepository;
        $this->serviceCounterRepository = $serviceCounterRepository;
        $this->serviceRepository = $serviceRepository;
        $this->sliderRepository = $sliderRepository;
        $this->stageRepository = $stageRepository;
        $this->teamRepository = $teamRepository;
        $this->menuRepository = $menuRepository;
        $this->reviewRepository = $reviewRepository;
        $this->modelTypeRepository = $modelTypeRepository;
    }

    /**
     * @return Advantage[]
     */
    public function findAdvantages(): array
    {
        return $this->advantageRepository->findVisible();
    }

    /**
     * @return Model[]
     */
    public function findModels(): array
    {
        return $this->modelRepository->findVisible();
    }

    /**
     * @return Quotation[]
     */
    public function findQuotation(): array
    {
        return $this->quotationRepository->findVisible();
    }

    /**
     * @return ServiceCounter[]
     */
    public function findServiceCounters(): array
    {
        return $this->serviceCounterRepository->findVisible();
    }

    /**
     * @return Service[]
     */
    public function findServices(): array
    {
        return $this->serviceRepository->findVisible();
    }

    /**
     * @return Slider[]
     */
    public function findSliders(): array
    {
        return $this->sliderRepository->findVisible();
    }

    /**
     * @return Stage[]
     */
    public function findStages(): array
    {
        return $this->stageRepository->findVisible();
    }

    /**
     * @return Team[]
     */
    public function findTeams(): array
    {
        return $this->teamRepository->findVisible();
    }

    /**
     * @return Menu[]
     */
    public function findMenu(): array
    {
        return $this->menuRepository->findVisible();
    }

    /**
     * @return Review[]
     */
    public function findReviews(): array
    {
        return $this->reviewRepository->findVisible();
    }

    /**
     * @return ModelType[]
     */
    public function findModelTypes(): array
    {
        return $this->modelTypeRepository->findVisible();
    }
}