<?php


namespace app\services;


use app\repositories\CallbackRepository;

class CallbackService
{
    /**
     * @var CallbackRepository
     */
    protected $callbackRepository;

    /**
     * CallbackService constructor.
     * @param CallbackRepository $callbackRepository
     */
    public function __construct(CallbackRepository $callbackRepository)
    {
        $this->callbackRepository = $callbackRepository;
    }

    /**
     * @param array $params
     * @return callable
     */
    public function createCallback(array $params)
    {
        $callback = $this->callbackRepository->createCallback($params);
        $this->callbackRepository->save($callback);

        return $callback;
    }
}