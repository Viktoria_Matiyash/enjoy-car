<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template</h1>
    <br>
</p>

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      config/             contains application configurations
      controllers/        contains Web controller classes
      models/             contains model classes
      repositories/       contains repositories classes
      services/           contains services classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.0


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

**STEP 1:**
~~~
git clone https://gitlab.com/Viktoria_Matiyash/enjoy-car.git
~~~
**STEP 2:**
~~~
composer install
~~~
**STEP 3:**
~~~
create .env in root directory, example in .env.example
~~~
**STEP 4:**
~~~
php yii migrate
~~~

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv("DB_DSN"),
    'username' => getenv("DB_USER"),
    'password' => getenv("DB_PASSWORD"),
    'charset' => 'utf8',
];
```

**NOTES:**
- config files generates from .env.


ACCESS TO ADMIN
-------------
**Login:** admin

**Password:** 5605
