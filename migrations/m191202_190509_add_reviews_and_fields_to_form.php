<?php

use yii\db\Migration;

/**
 * Class m191202_190509_add_reviews_and_fields_to_form
 */
class m191202_190509_add_reviews_and_fields_to_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'hide' => $this->boolean()->defaultValue(true),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'text_1' => $this->text(),
            'text_2' => $this->text(),
            'creation_time' => $this->integer(),
            'sort' => $this->integer(),
            'img' => $this->string()
        ]);

        $this->createTable('model_types', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(true),
            'creation_time' => $this->integer(),
            'sort' => $this->integer()
        ]);

        $this->addColumn('callbacks', 'model_type_id', $this->integer());
        $this->addColumn('callbacks', 'year_of_issue_from', $this->integer());
        $this->addColumn('callbacks', 'year_of_issue_to', $this->integer());
        $this->addColumn('callbacks', 'turnkey_budget', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('reviews');
        $this->dropTable('model_types');
    }


}
