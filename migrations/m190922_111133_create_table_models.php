<?php

use yii\db\Migration;

/**
 * Class m190922_111133_create_table_models
 */
class m190922_111133_create_table_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('models', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'img' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer(),
            'auction_cost_price' => $this->string(100),
            'auction_fee' => $this->string(100),
            'bank_commission' => $this->string(100),
            'power_of_attorney_for_forwarder' => $this->string(100),
            'port_unloading' => $this->string(100),
            'customs_broker' => $this->string(100),
            'customs_clearance' => $this->string(100),
            'services' => $this->string(100),
            'estimated_shipping_cost' => $this->string(100),
            'total' => $this->string(100),
            'euro_5_certificate' => $this->string(100),
            'auto_registration_and_pension_fund' => $this->string(100),
            'price_in_enjoy_cars' => $this->string(100),
            'price_in_ukraine' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190922_111133_create_table_models cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190922_111133_create_table_models cannot be reverted.\n";

        return false;
    }
    */
}
