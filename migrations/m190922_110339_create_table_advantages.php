<?php

use yii\db\Migration;

/**
 * Class m190922_110339_create_table_advantages
 */
class m190922_110339_create_table_advantages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('advantages', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'description_1' => $this->string(255),
            'description_2' => $this->string(255),
            'svg_img' => $this->string(255),
            'sort' => $this->integer(),
            'hide' => $this->boolean()->defaultValue(false),
            'creation_time' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('advantages');
    }

}
