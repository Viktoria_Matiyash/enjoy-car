<?php

use yii\db\Migration;

/**
 * Class m191205_184314_add_translates
 */
class m191205_184314_add_translates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'reviews_our_clients',
            'value_1' => 'Отзывы наших клиентов',
            'value_2' => 'Отзывы наших клиентов'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
