<?php

use yii\db\Migration;

/**
 * Class m191014_154610_platform
 */
class m191014_154610_platform extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('platforms', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'auction_id' => $this->integer(),
            'port_price_1' => $this->integer()->defaultValue(0),
            'port_price_2' => $this->integer()->defaultValue(0),
            'port_price_3' => $this->integer()->defaultValue(0),
            'port_price_4' => $this->integer()->defaultValue(0),
            'port_price_5' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('platforms');
    }

}
