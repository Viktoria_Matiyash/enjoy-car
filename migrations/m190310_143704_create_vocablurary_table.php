<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vocablurary}}`.
 */
class m190310_143704_create_vocablurary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('vocabulary', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(),
            'value_1' => $this->string(),
            'value_2' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('vocabulary');
    }
}
