<?php

use yii\db\Migration;

/**
 * Class m191014_081143_calculator_extra_price_2
 */
class m191014_081143_calculator_extra_price_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 0,
            'price_to' => 99.99,
            'commission' => 1,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 100,
            'price_to' => 199.99,
            'commission' => 40,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 200,
            'price_to' => 299.99,
            'commission' => 60,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 300,
            'price_to' => 349.99,
            'commission' => 75,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 350,
            'price_to' => 399.99,
            'commission' => 90,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 400,
            'price_to' => 499.99,
            'commission' => 110,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 500,
            'price_to' => 599.99,
            'commission' => 165,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 600,
            'price_to' => 699.99,
            'commission' => 180,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 700,
            'price_to' => 799.99,
            'commission' => 195,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 800,
            'price_to' => 899.99,
            'commission' => 210,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 900,
            'price_to' => 999.99,
            'commission' => 225,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1000,
            'price_to' => 1099.99,
            'commission' => 270,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1100,
            'price_to' => 1199.99,
            'commission' => 285,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1200,
            'price_to' => 1299.99,
            'commission' => 295,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1300,
            'price_to' => 1399.99,
            'commission' => 305,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1400,
            'price_to' => 1499.99,
            'commission' => 320,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1500,
            'price_to' => 1599.99,
            'commission' => 335,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1600,
            'price_to' => 1699.99,
            'commission' => 355,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1700,
            'price_to' => 1799.99,
            'commission' => 365,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 1800,
            'price_to' => 1999.99,
            'commission' => 375,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 2000,
            'price_to' => 2199.99,
            'commission' => 390,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 2200,
            'price_to' => 2399.99,
            'commission' => 395,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 2400,
            'price_to' => 2499.99,
            'commission' => 410,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 2500,
            'price_to' => 2999.99,
            'commission' => 425,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 3000,
            'price_to' => 3499.99,
            'commission' => 465,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 3500,
            'price_to' => 3999.99,
            'commission' => 515,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 4000,
            'price_to' => 4499.99,
            'commission' => 540,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 4500,
            'price_to' => 4999.99,
            'commission' => 565,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 5000,
            'price_to' => 5999.99,
            'commission' => 590,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 6000,
            'price_to' => 7499.99,
            'commission' => 615,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 7500,
            'price_to' => 9999.99,
            'commission' => 640,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 10000,
            'price_to' => 14999.99,
            'commission' => 670,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 1,
            'price_from' => 15000,
            'price_to' => 1000000,
            'commission' => 80,
            'percent_from_cost' => 0.04
        ]);

        //type 2
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 0,
            'price_to' => 99.99,
            'commission' => 0,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 100,
            'price_to' => 499.99,
            'commission' => 39,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 500,
            'price_to' => 999.99,
            'commission' => 49,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 1000,
            'price_to' => 1499.99,
            'commission' => 69,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 1500,
            'price_to' => 1999.99,
            'commission' => 79,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 2000,
            'price_to' => 3999.99,
            'commission' => 89,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 4000,
            'price_to' => 5999.99,
            'commission' => 99,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 6000,
            'price_to' => 7999.99,
            'commission' => 119,
        ]);
        $this->insert('extra_auction_prices', [
            'auction_id' => 2,
            'type' => 2,
            'price_from' => 8000,
            'price_to' => 1000000,
            'commission' => 129,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('extra_auction_prices');

    }


}
