<?php

use yii\db\Migration;

/**
 * Class m190925_171440_alter_table
 */
class m190925_171440_alter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\ServiceCounter::tableName(), 'img', $this->string(255));
        $this->addColumn(\app\models\Model::tableName(), 'cost_of_repair', $this->string(255));
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'cost_repair',
            'value_1' => 'Стоимость ремонта UA',
            'value_2' => 'Стоимость ремонта RU'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(\app\models\ServiceCounter::tableName(), 'img');
        $this->dropColumn(\app\models\Model::tableName(), 'cost_of_repair');
    }
}
