<?php

use yii\db\Migration;

/**
 * Class m191014_160511_add
 */
class m191014_160511_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('admins_menu', [
            'name' => 'Аукционы',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=auctions',
            'under' => 7,
            'sort' => 16,
            'creation_time' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('admins_menu', ['name' => 'Платформы']);
    }
}
