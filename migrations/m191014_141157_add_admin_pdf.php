<?php

use yii\db\Migration;

/**
 * Class m191014_141157_add_admin_pdf
 */
class m191014_141157_add_admin_pdf extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('admins_menu', [
            'name' => 'PDF`s',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=pdfs',
            'under' => 7,
            'sort' => 14,
            'creation_time' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('admins_menu', ['name' => 'PDF`s']);
    }


}
