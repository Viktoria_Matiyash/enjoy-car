<?php

use yii\db\Migration;

/**
 * Class m191014_155312_add
 */
class m191014_155312_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('admins_menu', [
            'name' => 'Платформы',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=platforms',
            'under' => 7,
            'sort' => 15,
            'creation_time' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('admins_menu', ['name' => 'Платформы']);
    }
}
