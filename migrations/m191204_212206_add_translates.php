<?php

use yii\db\Migration;

/**
 * Class m191204_212206_add_translates
 */
class m191204_212206_add_translates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('model_types', [
            'name_1' => 'Седан',
            'name_2' => 'Седан',
            'hide' => false
        ]);
        $this->insert('model_types', [
            'name_1' => 'Купе',
            'name_2' => 'Купе',
            'hide' => false
        ]);
        $this->insert('model_types', [
            'name_1' => 'Минивэн',
            'name_2' => 'Минивэн',
            'hide' => false
        ]);
        $this->insert('model_types', [
            'name_1' => 'Внедорожник',
            'name_2' => 'Внедорожник',
            'hide' => false
        ]);
        $this->insert('model_types', [
            'name_1' => 'Пикап',
            'name_2' => 'Пикап',
            'hide' => false
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'what_car_you_want',
            'value_1' => 'Какой автомобиль вас интересует?',
            'value_2' => 'Какой автомобиль вас интересует?'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'type_of_car',
            'value_1' => 'Тип Кузова',
            'value_2' => 'Тип Кузова'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'issue_from_year',
            'value_1' => 'Год выпуска (от)',
            'value_2' => 'Год выпуска (от)'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'issue_to_year',
            'value_1' => 'Год выпуска (до)',
            'value_2' => 'Год выпуска (до)'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'budget_by_key',
            'value_1' => 'Бюджет под ключ',
            'value_2' => 'Бюджет под ключ'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'get_the_auto',
            'value_1' => 'Подобрать авто',
            'value_2' => 'Подобрать авто'
        ]);

        $this->dropColumn('callbacks', 'turnkey_budget');
        $this->addColumn('callbacks', 'turnkey_budget', $this->string(255));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
