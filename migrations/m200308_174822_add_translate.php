<?php

use yii\db\Migration;

/**
 * Class m200308_174822_add_translate
 */
class m200308_174822_add_translate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'we_delivery_max',
            'value_1' => 'Мы привозим только <span>максимально</span> выгодные, проверенные и ликвидные авто из США для Вас',
            'value_2' => 'Мы привозим только <span>максимально</span> выгодные, проверенные и ликвидные авто из США для Вас'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => '10+stages',
            'value_1' => '10+ этапов авторского <br>подбора автомобиля',
            'value_2' => '10+ этапов авторского <br>подбора автомобиля'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'garantee_contract',
            'value_1' => 'Гарантируем конечную <br>цену авто в договоре',
            'value_2' => 'Гарантируем конечную <br>цену авто в договоре'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'good_price',
            'value_1' => 'Конкурентная цена <br>на доставку в Украину',
            'value_2' => 'Конкурентная цена <br>на доставку в Украину'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'get_good_price_by_3_cars',
            'value_1' => 'Получить расчет <br><span>3-х лучших автомобилей</span><br> в Ваш бюджет от наших экспертов',
            'value_2' => 'Получить расчет <br><span>3-х лучших автомобилей</span><br> в Ваш бюджет от наших экспертов'
        ]);

    }
}
