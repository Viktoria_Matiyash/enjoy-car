<?php

use yii\db\Migration;

/**
 * Class m190922_112805_create_table_feedbacks
 */
class m190922_112805_create_table_feedbacks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('callbacks', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'phone' => $this->string(255),
            'name_model' => $this->string(255),
            'creation_time' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('callbacks');
    }

}
