<?php

use yii\db\Migration;

/**
 * Class m190922_110741_create_table_quotations
 */
class m190922_110741_create_table_quotations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('quotations', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(500),
            'name_2' => $this->string(500),
            'background' => $this->string(255),
            'car_img' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('quotations');
    }

}
