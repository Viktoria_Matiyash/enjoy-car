<?php

use yii\db\Migration;

/**
 * Class m191014_135908_add_pdf_db
 */
class m191014_135908_add_pdf_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pdfs', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(null),
            'file' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pdfs');
    }

}
