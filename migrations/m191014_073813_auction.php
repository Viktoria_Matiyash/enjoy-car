<?php

use yii\db\Migration;

/**
 * Class m191014_073813_auction
 */
class m191014_073813_auction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auctions', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'cost_service' => $this->integer()
        ]);

        $this->insert('auctions', [
            'name' => 'IAAI',
            'cost_service' => 59
        ]);

        $this->insert('auctions', [
            'name' => 'Copart',
            'cost_service' => 39
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('auctions');
    }
}
