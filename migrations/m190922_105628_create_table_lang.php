<?php

use yii\db\Migration;

/**
 * Class m190922_105628_create_table_lang
 */
class m190922_105628_create_table_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lang', [
            'id' => $this->primaryKey(),
            'url' => $this->string(255),
            'local' => $this->string(255),
            'name' => $this->string(255),
            'default' => $this->boolean()->defaultValue(false),
        ]);

        $this->insert('lang', [
            'url' => 'ua',
            'local' => 'UA-ua',
            'name' => 'Укр',
            'default' => true
        ]);
        $this->insert('lang', [
            'url' => 'ru',
            'local' => 'RU-ru',
            'name' => 'Рус',
            'default' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lang');
    }


}
