<?php

use yii\db\Migration;

/**
 * Class m190922_154530_add_texts
 */
class m190922_154530_add_texts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'title',
            'value_1' => 'Title UA',
            'value_2' => 'Title RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'first_phone',
            'value_1' => '+38 (073) 535 25 15',
            'value_2' => '+38 (073) 535 25 15'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'second_phone',
            'value_1' => '+38 (097) 535 25 15',
            'value_2' => '+38 (097) 535 25 15'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'third_phone',
            'value_1' => '+38 (095) 525 25 15',
            'value_2' => '+38 (095) 525 25 15'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'facebook_link',
            'value_1' => 'facebook_link UA',
            'value_2' => 'facebook_link RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'get_call',
            'value_1' => 'Связаться с нами UA',
            'value_2' => 'Связаться с нами RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'youtube_link',
            'value_1' => 'youtube_link UA',
            'value_2' => 'youtube_link RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'telegram_link',
            'value_1' => 'telegram_link UA',
            'value_2' => 'telegram_link RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'instagram_link',
            'value_1' => 'instagram_link UA',
            'value_2' => 'instagram_link RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'all_defence',
            'value_1' => 'EnjoyCars 2019. Все права защищены UA',
            'value_2' => 'EnjoyCars 2019. Все права защищены RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'why_in_us',
            'value_1' => 'Почему заказывают у нас UA',
            'value_2' => 'Почему заказывают у нас RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'stage_of_auto',
            'value_1' => 'Этапы привоза авто UA',
            'value_2' => 'Этапы привоза авто RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'popular_models',
            'value_1' => 'Популярные модели UA',
            'value_2' => 'Популярные модели RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'cost_auto_USA',
            'value_1' => 'Стоимость авто на аукционе UA',
            'value_2' => 'Стоимость авто на аукционе RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'auction_cost',
            'value_1' => 'Аукционный сбор UA',
            'value_2' => 'Аукционный сбор RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'commission_bank',
            'value_1' => 'Комиссия банка за перевод денег в США UA',
            'value_2' => 'Комиссия банка за перевод денег в США RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'power_of_accerdetor',
            'value_1' => 'Доверенность на экспедитора UA',
            'value_2' => 'Доверенность на экспедитора RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'port_unloading',
            'value_1' => 'Разгрузка в порту UA',
            'value_2' => 'Разгрузка в порту RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'custom_broker',
            'value_1' => 'Таможенный брокер UA',
            'value_2' => 'Таможенный брокер RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'custom_clearance',
            'value_1' => 'Растаможка UA',
            'value_2' => 'Растаможка RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'services',
            'value_1' => 'Услуги UA',
            'value_2' => 'Услуги RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'estimate_shipping_cost',
            'value_1' => 'Предварительная стоимость доставки UA',
            'value_2' => 'Предварительная стоимость доставки RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'total',
            'value_1' => 'Итого UA',
            'value_2' => 'Итого RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'certificate_5_euro',
            'value_1' => 'Получение сертификата ЕВРО 5 UA',
            'value_2' => 'Получение сертификата ЕВРО 5 RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'registration_auto',
            'value_1' => 'Регистрация авто и пенсионный фонд UA',
            'value_2' => 'Регистрация авто и пенсионный фонд RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'price_enjoy_cars',
            'value_1' => 'Цена в EnjoyCars UA',
            'value_2' => 'Цена в EnjoyCars RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'price_ukraine',
            'value_1' => 'Цена в Украине UA',
            'value_2' => 'Цена в Украине RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'packages_services',
            'value_1' => 'Пакеты услуг UA',
            'value_2' => 'Пакеты услуг RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'packages_services_additional',
            'value_1' => 'Дополнтиельный текст о трех пакетах “забота о вашем удобстве UA',
            'value_2' => 'Дополнтиельный текст о трех пакетах “забота о вашем удобстве RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'team',
            'value_1' => 'Команда UA',
            'value_2' => 'Команда RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'more',
            'value_1' => 'Подробнее UA',
            'value_2' => 'Подробнее RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'link_to_telegram',
            'value_1' => 'Написать в Telegram UA',
            'value_2' => 'Написать в Telegram RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'validate_name',
            'value_1' => 'Это поле должно содержать только кириллицу UA',
            'value_2' => 'Это поле должно содержать только кириллицу RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'validate_phone',
            'value_1' => 'Это поле должно содержать телефон в формате +38 (123) 456-78-90 UA',
            'value_2' => 'Это поле должно содержать телефон в формате +38 (123) 456-78-90 RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'message_sent',
            'value_1' => 'Сообщение отправлено UA',
            'value_2' => 'Сообщение отправлено RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'our_contacts',
            'value_1' => 'Наши контакты UA',
            'value_2' => 'Наши контакты RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'phone',
            'value_1' => 'Телефон UA',
            'value_2' => 'Телефон RU'
        ]);
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'email',
            'value_1' => 'Email UA',
            'value_2' => 'Email RU'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'enjoy_email',
            'value_1' => 'enjoycars@gmail.com',
            'value_2' => 'enjoycars@gmail.com'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'address',
            'value_1' => 'Адрес',
            'value_2' => 'Адрес'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'our_address',
            'value_1' => 'Киев, ул. Кожемяцкая, 10-а, офис 3, 1 этаж',
            'value_2' => 'Киев, ул. Кожемяцкая, 10-а, офис 3, 1 этаж'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'good_text',
            'value_1' => 'Снимите с себя головную боль и позвольте нам
                <span>подобрать авто Вашей мечты!</span> UA',
            'value_2' => 'Снимите с себя головную боль и позвольте нам
                <span>подобрать авто Вашей мечты!</span>m RU'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }


}
