<?php

use yii\db\Migration;

/**
 * Class m190922_110854_create_table_stage
 */
class m190922_110854_create_table_stage extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('stages', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'description_1' => $this->string(255),
            'description_2' => $this->string(255),
            'img' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('stages');
    }

}
