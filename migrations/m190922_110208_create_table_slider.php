<?php

use yii\db\Migration;

/**
 * Class m190922_110208_create_table_slider
 */
class m190922_110208_create_table_slider extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sliders', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'background' => $this->string(255),
            'car_img' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer(),
            'creation_time' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sliders');
    }

}
