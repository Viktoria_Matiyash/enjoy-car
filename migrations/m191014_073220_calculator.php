<?php

use yii\db\Migration;

/**
 * Class m191014_073220_calculator
 */
class m191014_073220_calculator extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ports', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'delivery_cost' => $this->integer()
        ]);

        $this->insert('ports', [
            'name' => 'Savannah, GA',
            'delivery_cost' => 950
        ]);

        $this->insert('ports', [
            'name' => 'Newark, NJ',
            'delivery_cost' => 950
        ]);

        $this->insert('ports', [
            'name' => 'Houston, TX',
            'delivery_cost' => 1150
        ]);

        $this->insert('ports', [
            'name' => 'Los Angeles, CA',
            'delivery_cost' => 1290
        ]);

        $this->insert('ports', [
            'name' => 'Indianapolis',
            'delivery_cost' => 1200
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ports');
    }

}
