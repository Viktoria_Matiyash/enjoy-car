<?php

use yii\db\Migration;

/**
 * Class m190922_112455_create_table_teams
 */
class m190922_112455_create_table_teams extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('teams', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'description_1' => $this->string(500),
            'description_2' => $this->string(500),
            'specialization_1' => $this->string(255),
            'specialization_2' => $this->string(255),
            'img' => $this->string(255),
            'link' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer()
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('teams');
    }

}
