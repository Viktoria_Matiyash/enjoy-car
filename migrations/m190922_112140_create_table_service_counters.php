<?php

use yii\db\Migration;

/**
 * Class m190922_112140_create_table_service_counters
 */
class m190922_112140_create_table_service_counters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('service_counters', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'number' => $this->integer(),
            'sign' => $this->string(2),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('service_counters');
    }


}
