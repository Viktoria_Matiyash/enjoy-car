<?php

use yii\db\Migration;

/**
 * Class m190922_104156_create_table_menu_items
 */
class m190922_104156_create_table_menu_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(255)->defaultValue(null),
            'name_1' => $this->string(255)->defaultValue(null),
            'name_2' => $this->string(255)->defaultValue(null),
            'sort' => $this->integer(),
            'hide' => $this->boolean()->defaultValue(false)
        ]);

        $this->insert('menu', [
            'alias' => 'advantages',
            'name_1' => 'Преимущества ua',
            'name_2' => 'Приемущества ru',
            'sort' => 1
        ]);
        $this->insert('menu', [
            'alias' => 'stage',
            'name_1' => 'Этапы привоза ua',
            'name_2' => 'Этапы привоза ru',
            'sort' => 2
        ]);
        $this->insert('menu', [
            'alias' => 'popular',
            'name_1' => 'Модели авто ua',
            'name_2' => 'Модели авто ru',
            'sort' => 3
        ]);

        $this->insert('menu', [
            'alias' => 'services',
            'name_1' => 'Пакеты услуг ua',
            'name_2' => 'Пакеты услуг ru',
            'sort' => 4
        ]);
        $this->insert('menu', [
            'alias' => 'counter',
            'name_1' => 'Показатели ua',
            'name_2' => 'Показатели ru',
            'sort' => 5
        ]);
        $this->insert('menu', [
            'alias' => 'team',
            'name_1' => 'Команда ua',
            'name_2' => 'Команда ru',
            'sort' => 6
        ]);

        $this->insert('menu', [
            'alias' => 'feedback',
            'name_1' => 'Связаться ua',
            'name_2' => 'Связаться ru',
            'sort' => 7
        ]);
        $this->insert('menu', [
            'alias' => 'contacts',
            'name_1' => 'Контакты ua',
            'name_2' => 'Контакты ru',
            'sort' => 8
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu');
    }

}
