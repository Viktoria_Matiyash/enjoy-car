<?php

use yii\db\Migration;

/**
 * Class m200308_093710_add_translate
 */
class m200308_093710_add_translate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'ot_1000',
            'value_1' => '1000$',
            'value_2' => '1000$'
        ]);

        $this->insert(\app\models\Vocabulary::tableName(), [
            'alias' => 'vigognee_ot_rynka_ukraine',
            'value_1' => 'Выгодней от рынка Украины на',
            'value_2' => 'Выгодней от рынка Украины на'
        ]);
    }

}
