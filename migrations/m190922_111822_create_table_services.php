<?php

use yii\db\Migration;

/**
 * Class m190922_111822_create_table_services
 */
class m190922_111822_create_table_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('services', [
            'id' => $this->primaryKey(),
            'name_1' => $this->string(255),
            'name_2' => $this->string(255),
            'list_1' => $this->string(500),
            'list_2' => $this->string(500),
            'hide' => $this->boolean()->defaultValue(false),
            'sort' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('services');
    }

}
