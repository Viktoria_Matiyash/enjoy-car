<?php

use yii\db\Migration;

/**
 * Class m190922_112924_create_table_admins
 */
class m190922_112924_create_table_admins extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admins', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'passwd' => $this->string(255),
            'passwd_rec' => $this->string(255),
            'email' => $this->string(255),
            'deny_scripts' => $this->string(255)->defaultValue(null),
            'deny_tables' => $this->string(255)->defaultValue(null),
            'group_id' => $this->integer(),
            'creation_time' => $this->integer(),
        ]);
        $this->createTable('admins_groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'deny_restrict' => $this->string(255)->defaultValue(null),
            'deny_tables' => $this->string(255)->defaultValue(null),
            'under' => $this->integer(),
            'creation_time' => $this->integer(),
        ]);

        $this->createTable('admins_log', [
            'id' => $this->primaryKey(),
            'login' => $this->string(255),
            'ip' => $this->string(255),
            'table_name' => $this->string(255),
            'action' => $this->string(255),
            'rec_id' => $this->integer(),
            'creation_time' => $this->integer(),
        ]);

        $this->createTable('admins_menu', [
            'id' => $this->primaryKey(),
            'icon' => $this->string(255),
            'name' => $this->string(255),
            'url' => $this->string(255),
            'target' => $this->string(255)->null(),
            'under' => $this->integer()->defaultValue(-1),
            'sort' => $this->integer(),
            'creation_time' => $this->integer(),
        ]);

        $this->createTable('admins_menu_assoc', [
            'menu_id' => $this->integer(),
            'group_id' => $this->integer(),
        ]);

        $this->insert('admins', [
            'name' => 'Admin',
            'passwd' => 'bptkWmgSdZ3Wk',
            'passwd_rec' => '',
            'email' => 'admin@email.ru',
            'group_id' => 1,
            'creation_time' => time(),
        ]);

        $this->insert('admins_groups', [
            'name' => 'Admins',
            'under' => -1,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Администраторы',
            'icon' => 'glyphicon glyphicon-tower',
            'url' => 'catalog.php?tabler=admins_groups&tablei=admins',
            'under' => -1,
            'sort' => 1,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Настройки',
            'icon' => 'glyphicon glyphicon-cog',
            'url' => '',
            'under' => -1,
            'sort' => 2,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'SQL',
            'icon' => 'query.php',
            'url' => 'query.php',
            'under' => 2,
            'sort' => 3,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Словарь',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=vocabulary',
            'under' => 2,
            'sort' => 4,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Языки',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=lang',
            'under' => 2,
            'sort' => 5,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Меню',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=menu',
            'under' => 2,
            'sort' => 6,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Сайт',
            'icon' => '',
            'url' => '',
            'under' => -1,
            'sort' => 7,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Слайдер',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=sliders',
            'under' => 7,
            'sort' => 8,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Преимущества',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=advantages',
            'under' => 7,
            'sort' => 9,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Мотивационные фразы',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=quotations',
            'under' => 7,
            'sort' => 10,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Этапы',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=stages',
            'under' => 7,
            'sort' => 11,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Модели',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=models',
            'under' => 7,
            'sort' => 12,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Сервисы',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=services',
            'under' => 7,
            'sort' => 13,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Счетчики',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=service_counters',
            'under' => 7,
            'sort' => 14,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Команда',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=teams',
            'under' => 7,
            'sort' => 15,
            'creation_time' => time(),
        ]);

        $this->insert('admins_menu', [
            'name' => 'Обратная связь',
            'icon' => '',
            'url' => 'catalog.php?tabler=&tablei=callbacks',
            'under' => 7,
            'sort' => 16,
            'creation_time' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admins');
        $this->dropTable('admins_menu');
        $this->dropTable('admins_menu_assoc');
        $this->dropTable('admins_log');
        $this->dropTable('admins_groups');
    }

}
