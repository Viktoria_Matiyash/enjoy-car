<?php
Yii::setAlias('@pdf', dirname(__DIR__) . '/views/pdf');
Yii::setAlias('@userfiles', dirname(__DIR__) . '/userfiles/pdf');

Yii::$container->set(
    'app\repositories\MenuRepository',
    'app\repositories\MenuRepository',
    [
        \app\models\Menu::class
    ]
);

Yii::$container->set(
    'app\repositories\AdvantageRepository',
    'app\repositories\AdvantageRepository',
    [
        \app\models\Advantage::class
    ]
);

Yii::$container->set(
    'app\repositories\ModelRepository',
    'app\repositories\ModelRepository',
    [
        \app\models\Model::class
    ]
);

Yii::$container->set(
    'app\repositories\QuotationRepository',
    'app\repositories\QuotationRepository',
    [
        \app\models\Quotation::class
    ]
);

Yii::$container->set(
    'app\repositories\ServiceCounterRepository',
    'app\repositories\ServiceCounterRepository',
    [
        \app\models\ServiceCounter::class
    ]
);

Yii::$container->set(
    'app\repositories\ServiceRepository',
    'app\repositories\ServiceRepository',
    [
        \app\models\Service::class
    ]
);

Yii::$container->set(
    'app\repositories\SliderRepository',
    'app\repositories\SliderRepository',
    [
        \app\models\Slider::class
    ]
);

Yii::$container->set(
    'app\repositories\StageRepository',
    'app\repositories\StageRepository',
    [
        \app\models\Stage::class
    ]
);

Yii::$container->set(
    'app\repositories\TeamRepository',
    'app\repositories\TeamRepository',
    [
        \app\models\Team::class
    ]
);

Yii::$container->set(
    'app\repositories\CallbackRepository',
    'app\repositories\CallbackRepository',
    [
        \app\models\Callback::class
    ]
);

Yii::$container->set(
    \app\repositories\PortRepository::class,
    \app\repositories\PortRepository::class,
    [
        \app\models\Port::class
    ]
);

Yii::$container->set(
    \app\repositories\AuctionRepository::class,
    \app\repositories\AuctionRepository::class,
    [
        \app\models\Auction::class
    ]
);

Yii::$container->set(
    \app\repositories\CalculatorExtraPriceRepository::class,
    \app\repositories\CalculatorExtraPriceRepository::class,
    [
        \app\models\CalculatorExtraPrice::class
    ]
);

Yii::$container->set(
    \app\repositories\PdfParamsRepository::class,
    \app\repositories\PdfParamsRepository::class,
    [
        \app\models\PdfParams::class
    ]
);

Yii::$container->set(
    \app\repositories\PdfRepository::class,
    \app\repositories\PdfRepository::class,
    [
        \app\models\Pdf::class
    ]
);

Yii::$container->set(
    \app\repositories\PlatformRepository::class,
    \app\repositories\PlatformRepository::class,
    [
        \app\models\Platform::class
    ]
);

Yii::$container->set(
    \app\repositories\ReviewRepository::class,
    \app\repositories\ReviewRepository::class,
    [
        \app\models\Review::class
    ]
);

Yii::$container->set(
    \app\repositories\ModelTypeRepository::class,
    \app\repositories\ModelTypeRepository::class,
    [
        \app\models\ModelType::class
    ]
);