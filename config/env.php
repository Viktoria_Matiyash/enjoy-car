<?php
/**
 * Setup application environment
 */
$dotenv = new \Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();
if (!isset($_SESSION['admin_id'])) {
    session_start([
        'cookie_lifetime' => 86400,
    ]);
}
defined('YII_DEBUG') or define('YII_DEBUG', getenv('YII_DEBUG') === 'true');
defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV') ?: 'prod');
