<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Platform
 * @package app\models
 * @property $name string
 * @property $auction_id int
 */
class Platform extends ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'platforms';
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $portId
     * @return int
     */
    public function getDeliveryCost(int $portId): int
    {
        return $this->{'port_price_' . $portId};
    }

    /**
     * @return int
     */
    public function getAuctionId(): int
    {
        return $this->auction_id;
    }

    public function getId(): int
    {
        return $this->id;
    }

}