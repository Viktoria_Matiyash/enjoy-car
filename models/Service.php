<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name_1
 * @property string $name_2
 * @property string $list_1
 * @property string $list_2
 * @property string $img
 */
class Service extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    /**
     * @param int $langId
     * @return string
     */
    public function getList(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'list_' . $langId};
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return (string)Images::FOLDER . $this->img;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return (string)$this->number;
    }

    /**
     * @return string
     */
    public function getSign(): string
    {
        return (string)$this->sign;
    }

    public static function tableName()
    {
        return 'services';
    }

}