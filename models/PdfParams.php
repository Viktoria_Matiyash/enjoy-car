<?php

namespace app\models;

/**
 * Class PdfParams
 * @package app\models
 * @property float $price
 * @property float $auctionFee
 * @property float $delivery
 * @property float $freightForwarding
 * @property float $service
 * @property float $repair
 * @property float $certification
 * @property float $registration
 * @property float $customsClearance
 * @property float $deliveryToCityCost
 * @property string $auctionLogo
 * @property string $insurance
 */
class PdfParams extends \yii\base\Model
{
    public function fields()
    {
        return [
            'price',
            'auctionFee',
            'delivery',
            'freightForwarding',
            'service',
            'repair',
            'certification',
            'registration',
            'customsClearance',
            'deliveryToCityCost',
            'auctionLogo',
            'insurance',
        ];
    }

    public function attributes()
    {
        return [
            'price',
            'auctionFee',
            'delivery',
            'freightForwarding',
            'service',
            'repair',
            'certification',
            'registration',
            'customsClearance',
            'deliveryToCityCost',
            'auctionLogo',
            'insurance',
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'price',
                    'auctionFee',
                    'delivery',
                    'freightForwarding',
                    'service',
                    'repair',
                    'certification',
                    'registration',
                    'customsClearance',
                    'deliveryToCityCost',
                    'auctionLogo',
                    'insurance',
                ],
                'safe'
            ]
        ];
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getAuctionFee(): float
    {
        return $this->auctionFee;
    }

    /**
     * @return float
     */
    public function getDelivery(): float
    {
        return $this->delivery;
    }

    /**
     * @return float
     */
    public function getFreightForwarding(): float
    {
        return $this->freightForwarding;
    }

    /**
     * @return float
     */
    public function getService(): float
    {
        return $this->service;
    }

    /**
     * @return float
     */
    public function getRepair(): float
    {
        return $this->repair;
    }

    /**
     * @return float
     */
    public function getCertification(): float
    {
        return $this->certification;
    }

    /**
     * @return float
     */
    public function getRegistration(): float
    {
        return $this->registration;
    }

    /**
     * @return float
     */
    public function getCustomsClearance(): float
    {
        return $this->customsClearance;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param float $auctionFee
     */
    public function setAuctionFee(float $auctionFee): void
    {
        $this->auctionFee = $auctionFee;
    }

    /**
     * @param float $delivery
     */
    public function setDelivery(float $delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @param float $freightForwarding
     */
    public function setFreightForwarding(float $freightForwarding): void
    {
        $this->freightForwarding = $freightForwarding;
    }

    /**
     * @param float $service
     */
    public function setService(float $service): void
    {
        $this->service = $service;
    }

    /**
     * @param float $repair
     */
    public function setRepair(float $repair): void
    {
        $this->repair = $repair;
    }

    /**
     * @param float $certification
     */
    public function setCertification(float $certification): void
    {
        $this->certification = $certification;
    }

    /**
     * @param float $registration
     */
    public function setRegistration(float $registration): void
    {
        $this->registration = $registration;
    }

    /**
     * @param float $customsClearance
     */
    public function setCustomsClearance(float $customsClearance): void
    {
        $this->customsClearance = $customsClearance;
    }

    /**
     * @return float
     */
    public function getDeliveryToCityCost(): float
    {
        return $this->deliveryToCityCost;
    }

    /**
     * @param float $deliveryToCityCost
     */
    public function setDeliveryToCityCost(float $deliveryToCityCost): void
    {
        $this->deliveryToCityCost = $deliveryToCityCost;
    }

    /**
     * @return string
     */
    public function getAuctionLogo(): string
    {
        return $this->auctionLogo;
    }

    /**
     * @param string $auctionLogo
     */
    public function setAuctionLogo(string $auctionLogo): void
    {
        $this->auctionLogo = $auctionLogo;
    }

    public function getInsurance(): float
    {
        return $this->insurance;
    }

    public function setInsurance(float $insurance): void
    {
        $this->insurance = $insurance;
    }

    public function getAmount(): float
    {
        return (float)($this->getDeliveryToCityCost() + $this->getPrice() + $this->getAuctionFee() + $this->getDelivery() + $this->getFreightForwarding() + $this->getService() + $this->getRepair() + $this->getCertification() + $this->getRegistration() + $this->getCustomsClearance());
    }

}