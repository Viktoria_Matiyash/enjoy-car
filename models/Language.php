<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property int $default
 */
class Language extends ActiveRecord
{
    const LANGUAGE_UA = 1;
    const LANGUAGE_RU = 2;

    /**
     * @var null
     */
    static $current = null;


    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'lang';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function rules(): array
    {
        return [
            [['url', 'local', 'name'], 'required'],
            [['default'], 'integer'],
            [['url', 'local', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'local' => 'Local',
            'name' => 'Name',
            'default' => 'Default'
        ];
    }

    /**
     * @return Language
     */
    static function getCurrent(): Language
    {
        if (self::$current === null) {
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }


    /**
     * @param string|null $url
     */
    static function setCurrent(string $url = null): void
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->local;
    }

    /**
     * @return Language
     */
    static function getDefaultLang(): Language
    {
        return self::find()->where(['hide' => 0])->one();
    }

    /**
     * @param string|null $url
     * @return Language|array|null
     */
    static function getLangByUrl(string $url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = self::find()->where('url = :url', [':url' => $url])->one();
            if ($language === null) {
                return null;
            } else {
                return $language;
            }
        }
    }

    static function getLangIdByUrl(string $url = null)
    {
        $lang = self::find()->where('url = :url', [':url' => $url])->one();

        if (empty($url) || empty($lang)) {
            return self::LANGUAGE_UA;
        }

        return $lang->id;
    }

    public function getLangUrl()
    {
        $url = '';
        if ($this->default) {
            $url = Yii::$app->getRequest()->getLangUrl();
            if ($url == '') {
                $url = '/';
            }
        } else {
            $url = '/' . $this->url . Yii::$app->getRequest()->getLangUrl();

        }
        return $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function getPrepareUrl(): string
    {
        if (!$this->default) {
            return $this->url;
        }

        return '';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
