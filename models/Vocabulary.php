<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "vocabulary".
 *
 * @property int $id
 * @property string $alias
 * @property string $value_1
 * @property string $value_2
 */
class Vocabulary extends ActiveRecord
{

    public $value;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vocabulary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'value_1', 'value_2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'value_1' => 'Value 1',
            'value_2' => 'Value 2',
        ];
    }

}