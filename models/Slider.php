<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name_1
 * @property string $name_2
 * @property string $background
 * @property string $car_img
 */
class Slider extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    public function getDescription(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'description_' . $langId};
    }



    /**
     * @return string
     */
    public function getBackground(): string
    {
        return (string)Images::FOLDER . (Images::generateFormat($this->background)) . $this->background;
    }


    /**
     * @return string
     */
    public function getCatImg(): string
    {
        return (string)Images::FOLDER . (Images::generateFormat($this->car_img)) . $this->car_img;
    }


    public static function tableName()
    {
        return 'sliders';
    }

}