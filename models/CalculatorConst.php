<?php

namespace app\models;

class CalculatorConst
{
    public const PETROL_ID = 1;

    public const DIESEL_ID = 2;

    public const CUSTOMS_VALUE = 400;

    public const PETROL_LOW_PRICE = 54;

    public const PETROL_LARGE_PRICE = 108;

    public const DIESEL_LOW_PRICE = 81;

    public const DIESEL_LARGE_PRICE = 162;

    public const DUTY = 0.1;

    public const PETROL_COEFFICIENT = 2.9;

    public const DIESEL_COEFFICIENT = 3.5;

    public const VAT = 0.2;

    public const CUSTOMS_CLEARANCE_PRICE = 50;

    public const PENSION_FUND_COEFFICIENT = 0.03;

    public const REGISTRATION_PRICE = 40;

    public const BROKERS = 800;

    public const CERTIFICATION = 300;

    public const AUCTION_CONST = 35;

    public const BUDGETS = [
        [
            'budgetAlias' => 'ot-7-000-do-10-000',
            'discountAlias' => 'ot-1500-do-3000'
        ],
        [
            'budgetAlias' => 'ot-10-000-do-15-000',
            'discountAlias' => 'ot-2500-do-4500'
        ],
        [
            'budgetAlias' => 'ot-15-000-do-20-000',
            'discountAlias' => 'ot-3000-do-5500'
        ],
        [
            'budgetAlias' => 'ot-20-000-do-25-000',
            'discountAlias' => 'ot-4000-do-6500'
        ],
        [
            'budgetAlias' => 'ot-25-000-do-30-000',
            'discountAlias' => 'ot-6500-do-10000'
        ],
        [
            'budgetAlias' => 'ot-30-000-do-40-000',
            'discountAlias' => 'ot-8500-do-13000'
        ],
        [
            'budgetAlias' => 'ot-40-000-do-50-000',
            'discountAlias' => 'ot-15000'
        ],
        [
            'budgetAlias' => 'vyshe-50-000',
            'discountAlias' => 'ot-15000'
        ],
    ];
}