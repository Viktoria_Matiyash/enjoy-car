<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name
 * @property string $phone
 * @property string $creation_time
 */
class Callback extends ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'callbacks';
    }


    public function fields()
    {
        return [
            'id',
            'name',
            'phone',
            'name_model',
            'targetUrl',
            'year_of_issue_from',
            'year_of_issue_to',
            'turnkey_budget',
            'model_type_id'
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'name_model', 'targetUrl', 'turnkey_budget'], 'string'],
            [['model_type_id', 'year_of_issue_from', 'year_of_issue_to'], 'integer'],
            ['creation_time', 'default', 'value' => date('U')]
        ];
    }

}