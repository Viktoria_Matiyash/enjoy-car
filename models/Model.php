<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name_1
 * @property string $name_2
 * @property string $img
 * @property string $auction_cost_price
 * @property string $auction_fee
 * @property string $bank_commission
 * @property string $power_of_attorney_for_forwarder
 * @property string $port_unloading
 * @property string $customs_broker
 * @property string $customs_clearance
 * @property string $services
 * @property string $estimated_shipping_cost
 * @property string $total
 * @property string $euro_5_certificate
 * @property string $auto_registration_and_pension_fund
 * @property string $price_in_enjoy_cars
 * @property string $price_in_ukraine
 * @property string $cost_of_repair
 */
class Model extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return (string)Images::FOLDER . Images::generateFormat($this->img) . $this->img;
    }


    public static function tableName()
    {
        return 'models';
    }

    /**
     * @return string
     */
    public function getAuctionCostPrice(): string
    {
        return (string)$this->auction_cost_price;
    }

    /**
     * @return string
     */
    public function getAuctionFee(): string
    {
        return (string)$this->auction_fee;
    }

    /**
     * @return string
     */
    public function getBankCommission(): string
    {
        return (string)$this->bank_commission;
    }

    /**
     * @return string
     */
    public function getCustomsBroker(): string
    {
        return (string)$this->customs_broker;
    }

    /**
     * @return string
     */
    public function getCustomsClearance(): string
    {
        return (string)$this->customs_clearance;
    }

    /**
     * @return string
     */
    public function getServices(): string
    {
        return (string)$this->services;
    }

    /**
     * @return string
     */
    public function getEstimatedShippingCost(): string
    {
        return (string)$this->estimated_shipping_cost;
    }

    /**
     * @return string
     */
    public function getEuro5Certificate(): string
    {
        return (string)$this->euro_5_certificate;
    }

    /**
     * @return string
     */
    public function getAutoRegistrationAndPensionFund(): string
    {
        return (string)$this->auto_registration_and_pension_fund;
    }

    /**
     * @return string
     */
    public function getPowerOfAttorneyForForwarder(): string
    {
        return (string)$this->power_of_attorney_for_forwarder;
    }

    /**
     * @return string
     */
    public function getPortUnloading(): string
    {
        return (string)$this->port_unloading;
    }

    /**
     * @return string
     */
    public function getTotal(): string
    {
        return (string)$this->total;
    }

    /**
     * @return string
     */
    public function getPriceInEnjoyCars(): string
    {
        return (string)$this->price_in_enjoy_cars;
    }

    /**
     * @return string
     */
    public function getPriceInUkraine(): string
    {
        return (string)$this->price_in_ukraine;
    }

    /**
     * @return string
     */
    public function getCostOfRepair(): string
    {
        return (string)$this->cost_of_repair;
    }

    /**
     * @param string $cost_of_repair
     */
    public function setCostOfRepair(string $cost_of_repair): void
    {
        $this->cost_of_repair = $cost_of_repair;
    }



}