<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name_1
 * @property string $name_2
 * @property string $description_1
 * @property string $description_2
 * @property string $svg_img
 */
class Advantage extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    /**
     * @param int $langId
     * @return string
     */
    public function getDescription(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'description_' . $langId};
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return (string)Images::FOLDER . Images::generateFormat($this->svg_img) . $this->svg_img;
    }


    public static function tableName()
    {
        return 'advantages';
    }

}