<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Pdf
 * @package app\models
 * @property $name string
 * @property $file string
 */
class Pdf extends ActiveRecord
{
    public static function tableName()
    {
        return 'pdfs';
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'file' => function () {
                return \sprintf('/userfiles/pdf/%s', $this->file);
            }
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'name',
            'file'
        ];
    }

    public function rules()
    {
        return [[['name', 'file'], 'string']];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }


}