<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Port
 * @package app\models
 * @property int $id
 * @property int $cost_service
 * @property string $name
 */
class Auction extends ActiveRecord
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCostService(): int
    {
        return $this->cost_service;
    }


    /**
     * @return string
     */
    public static function tableName()
    {
        return 'auctions';
    }
}