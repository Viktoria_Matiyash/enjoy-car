<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $name_1
 * @property string $name_2
 * @property string $alias
 */
class Menu extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return (string)$this->alias;
    }


    public static function tableName()
    {
        return 'menu';
    }

}