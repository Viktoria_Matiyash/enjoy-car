<?php

namespace app\models;

use app\components\Images;
use yii\db\ActiveRecord;

class Review extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    /**
     * @param int $langId
     * @return string
     */
    public function getDescription(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'text_' . $langId};
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return (string)Images::FOLDER . Images::generateFormat($this->img) . $this->img;
    }

    public static function tableName()
    {
        return 'reviews';
    }
}
