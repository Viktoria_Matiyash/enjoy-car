<?php

namespace app\models;

use yii\db\ActiveRecord;

class ModelType extends ActiveRecord
{
    /**
     * @param int $langId
     * @return string
     */
    public function getName(int $langId = Language::LANGUAGE_UA): string
    {
        return (string)$this->{'name_' . $langId};
    }

    public function getId(): int
    {
        return $this->id;
    }

    public static function tableName()
    {
        return 'model_types';
    }
}