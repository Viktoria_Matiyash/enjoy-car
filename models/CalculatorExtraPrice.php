<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Port
 * @package app\models
 * @property int $id
 * @property int $auction_id
 * @property float $price_from
 * @property float $price_to
 * @property float $commission
 * @property float $percent_from_cost
 */
class CalculatorExtraPrice extends ActiveRecord
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAuctionId(): int
    {
        return $this->auction_id;
    }

    /**
     * @param int $auction_id
     */
    public function setAuctionId(int $auction_id): void
    {
        $this->auction_id = $auction_id;
    }

    /**
     * @return float
     */
    public function getPriceFrom(): float
    {
        return $this->price_from;
    }

    /**
     * @param float $price_from
     */
    public function setPriceFrom(float $price_from): void
    {
        $this->price_from = $price_from;
    }

    /**
     * @return float
     */
    public function getPriceTo(): float
    {
        return $this->price_to;
    }

    /**
     * @param float $price_to
     */
    public function setPriceTo(float $price_to): void
    {
        $this->price_to = $price_to;
    }

    /**
     * @return float
     */
    public function getCommission(): float
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return float
     */
    public function getPercentFromCost(): float
    {
        return $this->percent_from_cost;
    }

    /**
     * @param float $percent_from_cost
     */
    public function setPercentFromCost(float $percent_from_cost): void
    {
        $this->percent_from_cost = $percent_from_cost;
    }


    /**
     * @return string
     */
    public static function tableName()
    {
        return 'extra_auction_prices';
    }
}