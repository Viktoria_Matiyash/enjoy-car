<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Port
 * @package app\models
 * @property int $id
 * @property int $delivery_cost
 * @property string $name
 */
class Port extends ActiveRecord
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getDeliveryCost(): int
    {
        return $this->delivery_cost;
    }


    /**
     * @return string
     */
    public static function tableName()
    {
        return 'ports';
    }
}