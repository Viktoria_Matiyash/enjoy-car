<?php

namespace app\repositories;

use app\models\Auction;
use yii\db\ActiveQuery;

class AuctionRepository extends ActiveQuery
{
    /**
     * @return Auction[]
     */
    public function findAll(): array
    {
        return $this->orderBy(['id' => SORT_ASC])->all();
    }

    /**
     * @param int $id
     * @return Auction|null
     */
    public function findById(int $id): ?Auction
    {
        return $this->where(['id' => $id])->one();
    }
}