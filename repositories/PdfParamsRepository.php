<?php

namespace app\repositories;

use app\models\PdfParams;
use yii\db\ActiveQuery;

class PdfParamsRepository extends ActiveQuery
{
    /**
     * @param array $data
     * @return PdfParams
     */
    public function createPdfParams(array $data): PdfParams
    {
        $pdfParams = new $this->modelClass;
        $pdfParams->load($data, '');

        return $pdfParams;
    }
}