<?php

namespace app\repositories;

use app\models\CalculatorExtraPrice;
use yii\db\ActiveQuery;

class CalculatorExtraPriceRepository extends ActiveQuery
{
    public const BUYER_FEE_TYPE = 1;

    public const INTERNET_FEE_TYPE = 2;

    /**
     * @param int $type
     * @param int $auctionId
     * @param float $price
     * @return CalculatorExtraPrice|null
     */
    public function findByParams(int $type, int $auctionId, float $price): ?CalculatorExtraPrice
    {
        $query = clone $this;
        return $query->andWhere([
            'type' => $type,
            'auction_id' => $auctionId
        ])->andWhere($price . ' BETWEEN price_from AND price_to')->one();
    }

}