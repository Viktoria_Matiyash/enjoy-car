<?php

namespace app\repositories;

use app\models\Pdf;
use yii\db\ActiveQuery;

class PdfRepository extends ActiveQuery
{
    /**
     * @param $params
     * @return Pdf
     */
    public function createPdf($params): Pdf
    {
        $pdf = new $this->modelClass;
        $pdf->load($params, '');

        return $pdf;
    }

    /**
     * @param Pdf $pdf
     */
    public function save(Pdf $pdf)
    {
        $pdf->save();
    }
}