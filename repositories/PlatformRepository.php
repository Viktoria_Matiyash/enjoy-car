<?php

namespace app\repositories;

use app\models\Platform;
use yii\db\ActiveQuery;

class PlatformRepository extends ActiveQuery
{
    /**
     * @return Platform[]
     */
    public function findAll(): array
    {
        return $this->orderBy(['auction_id' => SORT_DESC, 'name' => SORT_ASC])->all();
    }

    /**
     * @param int $auctionId
     * @return Platform[]
     */
    public function findByAuctionId(int $auctionId): array
    {
        return $this->andWhere(['auction_id' => $auctionId, 'name' => SORT_ASC])->all();
    }

    /**
     * @param int $id
     * @return Platform
     */
    public function findById(int $id): Platform
    {
        return $this->andWhere(['id' => $id])->one();
    }
}