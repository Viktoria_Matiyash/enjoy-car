<?php

namespace app\repositories;

use app\models\Port;
use yii\db\ActiveQuery;

class PortRepository extends ActiveQuery
{
    /**
     * @return Port[]
     */
    public function findAll(): array
    {
        return $this->orderBy(['id' => SORT_DESC])->all();
    }

    /**
     * @param int $id
     * @return Port|null
     */
    public function findById(int $id): ?Port
    {
        return $this->where(['id' => $id])->one();
    }
}