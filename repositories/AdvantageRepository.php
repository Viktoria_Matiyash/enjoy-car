<?php

namespace app\repositories;

use yii\db\ActiveQuery;

class AdvantageRepository extends ActiveQuery
{
    public function findVisible()
    {
        return $this->andWhere(['hide' => 0])->orderBy(['sort' => SORT_ASC])->all();
    }
}