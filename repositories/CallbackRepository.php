<?php

namespace app\repositories;

use app\models\Callback;
use yii\db\ActiveQuery;

class CallbackRepository extends ActiveQuery
{
    public function createCallback($params): Callback
    {
        $callback = new $this->modelClass;
        $callback->load($params, '');

        return $callback;
    }

    public function save(Callback $callback)
    {
        $callback->save();
    }
}