<?php

namespace app\repositories;

use yii\db\ActiveQuery;

class ReviewRepository extends ActiveQuery
{
    public function findVisible(): array
    {
        return $this->andWhere(['hide' => false])->orderBy(['sort' => SORT_ASC])->all();
    }
}