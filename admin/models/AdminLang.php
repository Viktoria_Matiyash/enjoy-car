<?php


class admin_lang extends AdminTable
{
    public $TABLE = 'lang';
    public $IMG_NUM = 0;
    public $ECHO_NAME = 'name';
    public $NAME = "Языки";
    public $NAME2 = "Язык";

    function __construct()
    {

        $this->fld = array(
            new Field("name", "Название", 1),
            new Field("local", "Локаль", 1),
            new Field("url", "Урл", 1),
            new Field("default", "Default", 6, array('showInList' => 1, 'editInList' => 1)),
        );

    }
}