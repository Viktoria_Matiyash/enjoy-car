<?php

class admin_vocabulary extends AdminTable
{

    public $TABLE = 'vocabulary';
    public $IMG_NUM = 0;
    public $ECHO_NAME = 'alias';
    public $SORT = 'alias DESC';

    public $NAME = "словарь";
    public $NAME2 = "словарь";

    public $MULTI_LANG = 0;

    function __construct()
    {
        $this->fld[] = new Field("value_1", "Значение UA", 1, ['showInList' => true]);
        $this->fld[] = new Field("value_2", "Значение RU", 1, ['showInList' => true]);
        $this->fld[] = new Field("alias", "ключ", 1, ['showInList' => true]);
    }

}
