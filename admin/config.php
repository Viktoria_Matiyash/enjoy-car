<?php
require_once(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../config/env.php');
error_reporting(0);
$DB_HOST = getenv("DB_HOST");
$DB_NAME = getenv("DB_NAME");
$DB_USER = getenv("DB_USER");
$DB_PASSWORD = getenv("DB_PASSWORD");

//Admin
$ALANG = 'ua';
$PROJECT_NAME = "Enjoy cars";
$ADMIN_SESSION_AUTH = 1;

//Tables
$TABLE_ITEMS = "catalog_products";

$TABLE_DOCS_RUBS = "docs_rubs";
$TABLE_DOCS = "docs";
$TABLE_NEWS_RUBS = "news_rubs";
$TABLE_NEWS = "news";

$TABLE_USERS_RUBS = "utypes";
$TABLE_USERS = "users";
$TABLE_MAIL = "emails";
$TABLE_TAGS = "tags";

$TABLE_ADMINS_GROUPS = "admins_groups";
$TABLE_ADMINS = "admins";
$TABLE_ADMINS_MENU = "admins_menu";
$TABLE_ADMINS_MENU_ASSOC = "admins_menu_assoc";
$TABLE_ADMINS_LOG = "admins_log";